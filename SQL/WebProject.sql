﻿-- スタート
START TRANSACTION;

DROP DATABASE WebProject;

-- データベース作成
CREATE DATABASE WebProject DEFAULT CHARACTER SET utf8;

USE WebProject;

-- テーブル作成

CREATE TABLE categories (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    category_method_id int(11) NOT NULL
    );

CREATE TABLE categories_method (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    name              varchar(255) NOT NULL UNIQUE
    );

INSERT INTO categories_method (name) VALUES
    ('Java'),
    ('C'),
    ('C++'),
    ('SQL'),
    ('Python'),
    ('C#'),
    ('Assembly'),
    ('JavaScript'),
    ('HTML/CSS'),
    ('PHP'),
    ('GO'),
    ('COBOL'),
    ('Kotlin'),
    ('Swift'),
    ('R'),
    ('TypeScript'),
    ('Git'),
    ('Error'),
    ('Forum'),
    ('Editer'),
    ('Beginner'),
    ('Tips'),
    ('404');

CREATE TABLE tags (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    page_id           int(11),
    forum_id          int(11),
    tag_method_id     int(11) NOT NULL
    );

CREATE TABLE tag_methods (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    name              varchar(255) NOT NULL UNIQUE
    );

CREATE TABLE icon_methods (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    name              varchar(255) NOT NULL UNIQUE,
    url               varchar(255) NOT NULL UNIQUE
    );
    
INSERT INTO icon_methods (name,URL) VALUE 
    ('starticon','baseline_account_circle_black_18dp.png'),
    ('biotech','baseline_biotech_black_18dp.png');
    
CREATE TABLE users (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    login_id           varchar(255) UNIQUE NOT NULL,
    password          varchar(256) NOT NULL,
    name              varchar(15) NOT NULL,
    create_date        date NOT NULL,
    icon_method_id    int(11) NOT NULL,
    authority_method_id         int(11) NOT NULL
    );
    
INSERT INTO users VALUES
    (null,'admin','2F2A9A51EA0257C57C234A288F73E6B7','PHPmyアドミン',now(),1,1),
    (null,'aaa','47BCE5C74F589F4867DBD57E9CA9F808','aaa',now(),1,2),
    (null,'testchara1','28DE83B9D754CEA982F7A6541B05F594','テストちゃん1号',now(),1,2),
    (null,'testchara2','0AEB65BE09A03BA45393E73AB4DCC2FB','テストちゃん2号',now(),1,2),
    (null,'testchara3','1B3C1E88C9C484A130224C6CECEB8F83','テストちゃん3号',now(),1,2)
    ;
    
CREATE TABLE authority_methods (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    name              varchar(255) UNIQUE NOT NULL
    );
    
INSERT INTO authority_methods (name) VALUES
    ('管理者'),
    ('一般');
    
CREATE TABLE forums (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    title             varchar(256) NOT NULL,
    page_id           int(11),
    category_id       int(11) NOT NULL,
    question          int(11) NOT NULL,
    creater_id        int(11) NOT NULL,
    create_date       datetime NOT NULL
    );
    
INSERT INTO forums (title, page_id, category_id,question, creater_id, create_date)VALUE
    ('Java',1000,1,0,1,'2020/09/25 10:20:52'),
    ('HTML/CSS',1001,1,0,1,'2020/09/25 11:20:52'),
    ('ぱげ１',1002,1,1,1,'2020/09/25 12:20:52'),
    ('ふぉるｍ１',0,1,0,1,'2020/09/25 13:23:13'),
    ('ぱげ２',1003,1,0,1,'2020/09/25 14:23:37'),
    ('ふぉるｍ２',0,1,0,1,'2020/09/25 15:23:50')
    ;
    
CREATE TABLE comments (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    forum_id          int(11) ,
    comment_id        int(11) ,
    creater_id        int(11) NOT NULL,
    text              longtext NOT NULL,
    textsub           longtext ,
    create_date       datetime NOT NULL
    );
    
CREATE TABLE pages (
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    title             varchar(256) NOT NULL,
    text              longtext NOT NULL,
    textsub           longtext ,
    creater_id        int(11) NOT NULL,
    category_id       int(11) NOT NULL,
    create_date       date NOT NULL
    )auto_increment = 1000;
    
INSERT INTO pages (title, text, textsub,creater_id, category_id, create_date)VALUE
    ('Java','aaa','aaa',1,1,'2020/09/20'),
    ('HTML/CSS','そこそこ','aaa',1,1,'2020/09/21'),
    ('ぱげ１','1こめ','1こめ',1,1,'2020/09/22'),
    ('ぱげ２','1こめ','1こめ',1,1,'2020/09/23')
    ;

CREATE TABLE primaries(
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    title             varchar(256) NOT NULL,
    detail            text NOT NULL,
    create_date       date NOT NULL,
    creater_id    int(11) NOT NULL
    );
    
INSERT INTO primaries (title, detail, create_date, creater_id)VALUE
    ('Java','大変',now(),1),
    ('HTML/CSS','そこそこ',now(),2)
    ;
    
CREATE TABLE secondaries(
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    primary_id       int(11) NOT NULL,
    title             varchar(256) NOT NULL,
    number            int(11) NOT NULL
    );
    
INSERT INTO secondaries (primary_id, title, number)VALUE
    (1,'基礎',1),
    (1,'応用',2),
    (1,'問題',4),
    (1,'発展問題',3),
    (2,'基礎',1),
    (2,'応用',2),
    (2,'問題',3)
    ;
    
CREATE TABLE tertiaries(
    id                int(11) PRIMARY KEY AUTO_INCREMENT,
    secondary_id      int(11) NOT NULL,
    title             varchar(255) NOT NULL,
    number            int(11) NOT NULL,
    page_id           int(11) NOT NULL
    );
    
INSERT INTO tertiaries (secondary_id, title, number,page_id)VALUE
    (1,'変数',1,1000),
    (1,'変数２',2,1001),
    (1,'関数',3,1002),
    (2,'オーバーライド',1,1003),
    (2,'プライベート',2,1004),
    (2,'連携',3,1005),
    (3,'基礎総合問題',1,1006),
    (4,'応用総合問題',2,1007),
    (4,'テトリス作成問題',3,1008)
    ;
    
COMMIT;
