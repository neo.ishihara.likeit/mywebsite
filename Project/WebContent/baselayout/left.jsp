<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="left">
	<h2>▼メニュー▼</h2>
	<div class="accordion" id="accordionExample">
		<div class="card" style="background-color: #bef3ff">
			<div class="card-header" id="headingOne">
				<h2 class="mb-0">
					<button class="btn btn-secondary btn-block" type="button"
						data-toggle="collapse" data-target="#collapseOne"
						aria-expanded="true" aria-controls="collapseOne">検索▼</button>
				</h2>
			</div>
			<div id="collapseOne" class="collapse show"
				aria-labelledby="headingOne" data-parent="#accordionExample">
				<div class="card-body" style="background-color: #a1c9ff">
					<form action="List" method="get">
						<div class="container">
							<div class="row">
								<input class="form-control col-lg-10" type="search"
									name="search_word" placeholder="Search" aria-label="Search">
								<button class="btn btn-outline-success col-lg-2" type="submit">🔍</button>
							</div>
						</div>
						<br> <a class="btn btn-primary" data-toggle="collapse"
							href="#collapse1" role="button" aria-expanded="false"
							aria-controls="collapse1" style="width: 100%;">
							<h4>カテゴリー検索</h4>
						</a>
						<div class="collapse" id="collapse1">
							<div class="card card-body" style="padding: 5% 0% 5% 0%;">
								<div class="container">
									<div class="selectbox">
										<c:forEach var="item" items="${calist}">
										<label class="category"><input type="radio" name="category"
											value=${item.id } style="width: 10%;"> ${item.name} </label>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
						<!--
						<a class="btn btn-primary" data-toggle="collapse"
							href="#collapse3" role="button" aria-expanded="false"
							aria-controls="collapse3" style="width: 100%;">
							<h5>タグ選択</h5>
						</a>
						<div class="collapse" id="collapse3">
							<div class="card card-body">
								<div class="container">
									<div class="selectbox">
										<label class="category"><input type="checkbox"
											value="1" style="width: 10%;"> Assembly</label> <label
											class="category"><input type="checkbox" value="1"
											style="width: 10%;"> TypeScript</label> <label
											class="category"><input type="checkbox" value="1"
											style="width: 10%;"> GO</label> <label class="category"><input
											type="checkbox" value="1" style="width: 10%;"> COBOL</label>
										<label class="category"><input type="checkbox"
											value="1" style="width: 10%;"> R</label> <label
											class="category"><input type="checkbox" value="1"
											style="width: 10%;"> Git</label> <label class="category"><input
											type="checkbox" value="1" style="width: 10%;"> 雑談</label> <label
											class="category"><input type="checkbox" value="1"
											style="width: 10%;"> エディター</label>
									</div>
								</div>
							</div>
						</div>
						-->
					</form>
				</div>
			</div>
		</div>
	</div>
	<br>
</div>