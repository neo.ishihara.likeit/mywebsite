<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<header>
	<div class="d-flex bd-highlight mb-3">
		<div class="p-2 bd-highlight">
			<a class="homebutton1 homebutton1-hover" href="Index">P</a><a
				class="homebutton1 homebutton1-hover" href="Index">r</a><a
				class="homebutton2 homebutton2-hover" href="Index">o</a><a
				class="homebutton1 homebutton1-hover" href="Index">r</a><a
				class="homebutton2 homebutton2-hover" href="Index">o</a><a
				class="homebutton1 homebutton1-hover" href="Index">a</a><a
				class="homebutton1 homebutton1-hover" href="Index">d</a>
		</div>






		<!-- セッションが表示切替 -->
		<c:if test="${userdata.authority_method_id == 1}" var="flg" />
		<c:if test="${flg}">
			<div class="p-2 bd-highlight">
				<form class="form-inline" action="PrimaryCreate" method="get">
					<button class="btn btn-outline-info" type="submit">コースの新規作成</button>
				</form>
			</div>
			<div class="p-2 bd-highlight">
				<form class="form-inline" action="ForumCreate" method="get">
					<button class="btn btn-outline-info" type="submit">フォーラムの新規作成</button>
				</form>
			</div>
			<div class="mr-auto p-2 bd-highlight">
				<form class="form-inline" action="PageCreate" method="get">
					<button class="btn btn-outline-info" type="submit">ページの新規作成</button>
				</form>
			</div>
		</c:if>
		<c:if test="${!flg}">
			<div class="mr-auto p-2 bd-highlight"></div>
		</c:if>

		<div class="p-2 bd-highlight">
			<form class="form-inline" action="List" method="get">
				<input class="form-control mr-sm-1 " type="search" name="search_word"
					placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success" type="submit">🔍</button>
			</form>
		</div>


		<!-- セッションの有無でログイン、ユーザー表示切替 -->
		<c:if test="${userdata == null}" var="flg" />
		<c:if test="${flg}">
			<div class="p-2 bd-highlight">
				<form class="form-inline" action="/MyWebSite/Login" method="get">
					<button class="btn btn-outline-info" type="submit">ログイン・サインイン</button>
				</form>
			</div>
		</c:if>
		<c:if test="${!flg}">
			<div class="p-2 bd-highlight">
				<form class="form-inline" action="/MyWebSite/User" method="get">
					<button class="btn btn-info" type="submit">
						<img style="width: 24px; height: 24px;"
							src="img/icon/${icondata.URL}">${userdata.name}
					</button>
				</form>
			</div>
		</c:if>
	</div>
</header>