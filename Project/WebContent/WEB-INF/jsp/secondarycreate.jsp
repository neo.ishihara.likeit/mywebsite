<%@page import="dao.PstDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="beans.*"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>

	<!-- 真ん中　-->
	<div id="bodder">


		<!-- メイン　-->
		<div id="middle">
			<!-- サイトマップ的な　-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="/home.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">リスト作成ページ</li>
				</ol>
			</nav>
			<!-- Main -->
			<main style="position: relative"> <!-- 削除ボタン -->
			<div class="datecorner">
				<form action="PstUpdate" method="get">
					<input type="hidden" name="primaryid" value="${primaryid}">
					<input type="submit" class="btn btn-danger" value="リストの作成を取り消す">
				</form>
			</div>
			<form action="SecondaryCreate" method="post">
				<!-- タイトル -->
				<h1>
					<input type=text placeholder="コース名" name="stitle" required>
				</h1>

				<!-- 説明 -->
				<input type="number" name="snuber" placeholder="表示する順番" min="1"
					max="2147483647" required> <br> <input type="hidden"
					name="primaryid" value="${primaryid}"> <input type="submit"
					value="この内容でリストを作成する">
			</form>
			</main>
		</div>
	</div>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--
	        (ｰ`)
      ／⌒⌒⌒⌒⌒ヽ
    ／／￣￣￣フ ／
  ／（＿＿＿／ ／
（＿＿＿＿＿_ノ

-->