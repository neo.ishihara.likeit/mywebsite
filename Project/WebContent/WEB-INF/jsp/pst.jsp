<%@page import="dao.PstDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="beans.*"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>

	<!-- 真ん中 -->
	<div id="bodder">


		<!-- 左メニュー -->
		<jsp:include page="/baselayout/left.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>


		<!-- メイン　-->
		<div id="middle">
			<!-- サイトマップ的な　-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="Index">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">${pst_p.title}</li>
				</ol>
			</nav>
			<!-- Main -->
			<main style="position: relative"> <!-- 日付 -->
			<div class="datecorner">${pst_p.create_date}<br>

				<c:if
					test="${userdata.authority_method_id == 1 || pst_p.creater_id == userdata.id}">
					<div>
						<form action="PstUpdate" method="get">
							<input type="hidden" name="primaryid" value="${pst_p.id}">
							<input type="submit" value="コースを編集">
						</form>
					</div>
				</c:if>
			</div>
			<!-- タイトル -->
			<h1>${pst_p.title}</h1>

			<!-- 説明 -->
			<p>${pst_p.detail}</p>

			<!-- メイン中身 -->

			<div class="pstbox">
				<c:forEach var="s" items="${pst_s}">
					<div class="selectionbox">
						<h3 style="border-bottom: 1px solid black">${s.number}.${s.title}</h3>
						<c:forEach var="t" items="${pst_t}">
							<c:if test="${s.id == t.secondary_id}">
								<form action="Page" method="get">
									<input type="hidden" name="page_id" value="${t.page_id}">
									<input type="submit" class="tertiarybutton" value="${t.number}.${t.title}">
								</form>
							</c:if>
						</c:forEach>
					</div>
				</c:forEach>
			</div>
			</main>
		</div>
		<!-- 右 -->
		<jsp:include page="/baselayout/right.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>
	</div>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--
	        (ｰ`)
      ／⌒⌒⌒⌒⌒ヽ
    ／／￣￣￣フ ／
  ／（＿＿＿／ ／
（＿＿＿＿＿_ノ

-->