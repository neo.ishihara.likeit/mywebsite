<%@page import="dao.PstDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="beans.*"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
	<!-- 真ん中　-->
	<div id="bodder">

		<!-- メイン　-->
		<div id="middle">
			<!-- サイトマップ的な　-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="Index">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">コース編集ページ</li>
				</ol>
			</nav>
			<!-- Main -->
			<main style="position: relative"> <!-- 削除ボタン -->
			<div class="datecorner">
				${primarydata.create_date }
				<form action="PrimaryUpdate" method="post">
					<input type="hidden" name="primarydelete" value="true"> <input
						type="hidden" name="primaryid" value="${primarydata.id}">
					<input type="submit" class="btn btn-danger" value="コースを削除する">
				</form>
			</div>
			<form action="PrimaryUpdate" method="post">
				<!-- タイトル -->
				<h1>
					<input type=text placeholder="コース名" name="ptitle"
						value="${primarydata.title}" required>
				</h1>

				<!-- 説明 -->
				<textarea placeholder="コースの説明" name="pdetail"
					style="width: 800px; min-height: 50px;" required>${primarydata.detail}</textarea>
				<br> <input type="hidden" name="primaryid"
					value="${primarydata.id}"> <input type="submit"
					value="この内容でコースを更新する">
			</form>
			</main>
		</div>
	</div>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--
	        (ｰ`)
      ／⌒⌒⌒⌒⌒ヽ
    ／／￣￣￣フ ／
  ／（＿＿＿／ ／
（＿＿＿＿＿_ノ

-->