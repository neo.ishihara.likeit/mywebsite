<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<header>
		<div class="d-flex bd-highlight mb-3">
			<div class="p-2 bd-highlight">
				<a class="homebutton1 homebutton1-hover" href="Index">P</a><a
					class="homebutton1 homebutton1-hover" href="Index">r</a><a
					class="homebutton2 homebutton2-hover" href="Index">o</a><a
					class="homebutton1 homebutton1-hover" href="Index">r</a><a
					class="homebutton2 homebutton2-hover" href="Index">o</a><a
					class="homebutton1 homebutton1-hover" href="Index">a</a><a
					class="homebutton1 homebutton1-hover" href="Index">d</a>
			</div>
		</div>
	</header>

	<!-- 真ん中　-->
	<div id="bodder">

		<!-- Main -->
		<main style="position: relative"> <!-- タイトル -->
		<h1 style="text-align: center;">ログイン・サインイン</h1>
		<!-- error --> <c:if test="${lerror != null}">
			<h2 style="text-align: center;">
				<font color="red"> ※どちらかが空です※</font>
			</h2>
		</c:if> <c:if test="${nouser != null}">
			<h2 style="text-align: center;">
				<font color="red"> ※ログインIDまたはパスワードが間違っています※</font>
			</h2>
		</c:if> <!-- メイン中身 --> <br>

		<div class="row" style="text-align: center;">
			<!-- Login -->
			<div style="width: 50%;">
				<div class="inputarea">
					<h2 style="border-bottom: 1px solid black;">
						ログイン<br>
						<br>
					</h2>
					<br>
					<form action="/MyWebSite/Login" method="post">
						<input type="hidden" name="login" value="1"> ${error}
						<h4>ログインID</h4>
						<input type="text" name="loginid" autofocus required> <br>
						<br>

						<h4>パスワード</h4>
						<input type="password" name="pass" required> <br>
						<br>
						<br>

						<button class="btn btn-outline-success" type="submit">ログイン</button>
						<br>
						<br>

					</form>
				</div>
			</div>
			<!-- Sign In -->
			<div style="width: 50%;">
				<div class="inputarea">
					<h2 style="border-bottom: 1px solid black;">
						サインイン<br>
						<br>
					</h2>
					<br>
					<form action="/MyWebSite/SigninConfirm" method="get">
						<input type="hidden" name="signin" value="1">
						<div style="display: flex;">
							<div style="width: 50%;">
								<h4>ログインID</h4>
								<input type="text" name="sloginid" autocomplete="new-password"
									value="${signuser.loginid}" required>
							</div>

							<div style="width: 50%;">
								<h4>名前</h4>
								<input type="text" name="sname" autocomplete="new-password"
									value="${signuser.name}" required>
							</div>
						</div>

						<br>

						<div style="display: flex;">
							<div style="width: 50%;">
								<h4>パスワード</h4>
								<input type="password" name="spass" autocomplete="new-password"
									required>
							</div>

							<div style="width: 50%;">
								<h4>パスワード再確認</h4>
								<input type="password" name="spasskaku"
									autocomplete="new-password" required>
							</div>
						</div>

						<br>
						<br>

						<button class="btn btn-outline-secondary" type="submit">新規アカウントを登録</button>
						<br>
						<br>

					</form>
				</div>
			</div>
		</div>
		</main>

	</div>

	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--
	   			  (ｰ`)
            ／⌒⌒⌒⌒⌒ヽ
          ／／￣￣￣フ ／
        ／（＿＿＿／ ／
      （＿＿＿＿＿_ノ

-->