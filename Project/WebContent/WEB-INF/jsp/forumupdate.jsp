<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
<!-- マークダウン -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
</head>
<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
	<!-- 真ん中 -->
	<div id="bodder">
		<!-- 真ん中 -->
		<div id="middle">
			<!-- サイトマップ的な-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="/home.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">ForumUpdate</li>
				</ol>
			</nav>
			<!-- Main -->
			<main> <!-- 日付 -->
			<div class="datecorner">
				作成日時：${forumdata.create_date}<br>
					<form action="ForumUpdate" method="post">
						<input type="hidden" name="forumdelete" value="true"> <input
							type="hidden" name="forum_id" value="${forumdata.id}"> <input
							type="submit" value="このフォーラムを削除">
					</form>
			</div>
			<form action="ForumUpdate" method="post">
				<!-- カテゴリー -->
				<div style="height: 50px; display: flex;">
					<div>
						カテゴリー：<select style="width: 100px" name="category" required>
							<c:forEach var="item" items="${calist}">
								<option value="${item.id}">${item.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<!-- 質問かどうか -->
				<c:if test="${forumdata.question == 0}">
					質問ではない<input type="radio" name="question" value="0" checked="checked"><br>
					質問受付中<input type="radio" name="question" value="1"><br>
					解決した<input type="radio" name="question" value="2">
				</c:if>
				<c:if test="${forumdata.question == 1}">
					質問ではない<input type="radio" name="question" value="0"><br>
					質問受付中<input type="radio" name="question" value="1" checked="checked"><br>
					解決した<input type="radio" name="question" value="2">
				</c:if>
				<c:if test="${forumdata.question == 2}">
					質問ではない<input type="radio" name="question" value="0"><br>
					質問受付中<input type="radio" name="question" value="1"><br>
					解決した<input type="radio" name="question" value="2" checked="checked">
				</c:if>
				<!-- タイトル -->
				<div>
					<h1>
						タイトル <input type="text" name="title" value="${forumdata.title}"maxlength="50"
							required>
					</h1>
				</div>
				<!-- メイン中身 -->
				<!-- コメント投稿 -->
				<div class="comment_update">
					<h5>コメントの投稿</h5>
					<!-- メイン中身 -->
					<div class="editor uk-container uk-margin-small">

						<textarea id="editor" name="markdown" rows="8" cols="40" required>${commentdata.textsub }</textarea>

						<button type="submit" onclick="draw_preview()"
							class="uk-button uk-button-default uk-margin-auto">この内容でフォーラムを更新する</button>

						<!-- HTMLに変換した結果 -->
						<div hidden class="col-md-12 ml-auto mr-auto text-left">
							<div class="preview" id="marked-preview"></div>
						</div>

						<!-- 変換結果コピペエリア -->
						<textarea hidden style="display: block;" onclick="this.select();"
							id="result" class="uk-textarea" name="result" rows="8" cols="100"
							readonly></textarea>
					</div>
					<input type="hidden" name="forum_id" value="${forumdata.id}">
				</div>
			</form>
			</main>
		</div>
	</div>
	<!-- マークダウンのスクリプト軍 -->
	<script
		src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/uikit.min.js"></script>
	<script>
		var simplemde = new SimpleMDE({
			element : document.getElementById("editor"),
			forceSync : true,
			spellChecker : false
		});
	</script>
	<script>
		marked.setOptions({
			sanitize : true,
			sanitizer : escape,
			breaks : true
		});

		function draw_preview() {
			var markdown = document.getElementById("editor").value;
			var html = marked(markdown);
			$('#marked-preview').html(html);
			$('#result').val(html);
		}
	</script>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>