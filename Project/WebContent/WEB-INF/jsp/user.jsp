<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<header>
		<div class="d-flex bd-highlight mb-3">
			<div class="p-2 bd-highlight">
				<a class="homebutton1 homebutton1-hover" href="Index">P</a><a
					class="homebutton1 homebutton1-hover" href="Index">r</a><a
					class="homebutton2 homebutton2-hover" href="Index">o</a><a
					class="homebutton1 homebutton1-hover" href="Index">r</a><a
					class="homebutton2 homebutton2-hover" href="Index">o</a><a
					class="homebutton1 homebutton1-hover" href="Index">a</a><a
					class="homebutton1 homebutton1-hover" href="Index">d</a>
			</div>
			<div class="ml-auto p-2 bd-highlight">
				<form action="User" method="post">
					<button class="btn btn-outline-secondary" type="submit">ログアウト</button>
				</form>
			</div>
		</div>
	</header>


	<!-- 真ん中　-->
	<div id="bodder">

		<!-- Main -->
		<main style="position: relative"> <!-- タイトル -->
		<h1 style="text-align: center;">ユーザーページ</h1>
		<!-- メイン中身 --> <br>
		<!--

                    自己紹介の追加


                    好きな言語３つ選べる機能の追加

                    二つ名、実績の追加

                -->
		<div class="row" style="text-align: center;">
			<!-- Sign In -->
			<div style="width: 100%;">
				<div class="inputarea">
					<div style="display: flex; border-bottom: 1px solid black;">
						<div style="width: 33%;">
							<h4>権限</h4>
							<h2>${authorityname}</h2>
						</div>
						<div style="width: 33%;">
							<h4>ログインID</h4>
							<h2>${userdata.loginid}</h2>
						</div>
						<div style="width: 33%;">
							<h4>ユーザー作成日</h4>
							<h2>${userdata.createdate}</h2>
						</div>
					</div>
					<br>
					<form action="/home.html">

						<div style="display: flex;">
							<div style="width: 50%;">
								<h4>アイコン</h4>
								<h2>
									<img src="img/icon/${icondata.URL}">
								</h2>
							</div>

							<div style="width: 50%;">
								<h4>名前</h4>
								<h2>${userdata.name}</h2>
							</div>
						</div>

						<br>

						<div style="display: flex;">
							<div style="width: 50%;">
								<h4>誕生日</h4>
								<h2>未定</h2>
							</div>
							<div style="width: 50%;">
								<h4>今までのコメント</h4>
								<h2>検索リストURL</h2>
							</div>
						</div>

						<br> <br> <a class="btn btn-outline-secondary"
							href="UserSetting">ユーザー情報変更</a><br> <br>

					</form>
				</div>
			</div>
		</div>
		</main>

	</div>

	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--       (ｰ`)
      ／⌒⌒⌒⌒⌒ヽ
    ／／￣￣￣フ ／
  ／（＿＿＿／ ／
（＿＿＿＿＿_ノ	-->