<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>

	<!-- 真ん中-->
	<div id="bodder">

		<!-- Main -->
		<main style="position: relative"> <!-- タイトル -->
		<h1 style="text-align: center;">この内容でアカウントを登録しますか？</h1>
		<!-- メイン中身 --> <br>

		<div class="row" style="text-align: center;">
			<!-- Sign In -->
			<div style="width: 100%;">
				<div class="inputarea">
					<div style="display: flex; border-bottom: 1px solid black;">
						<div style="width: 100%;">
							<h4>権限</h4>
							<h2>${authority}</h2>
						</div>
					</div>
					<br>
					<div style="display: flex;">
						<div style="width: 50%;">
							<h4>ログインID</h4>
							<h2>${signuser.loginid}</h2>
						</div>
						<div style="width: 50%;">
							<h4>名前</h4>
							<h2>${signuser.name}</h2>
						</div>
					</div>
					<br>
					<div style="display: flex;">
						<div style="width: 50%;">
							<h4>パスワード</h4>
							<div class="input-field col s10 offset-s1">
								<h2>
									<input type="password" readonly value="${signuser.password}">
								</h2>
							</div>
						</div>
						<div style="width: 50%;">
							<h4>ユーザー作成日</h4>
							<h2>${signuser.createdate}</h2>
						</div>
					</div>

					<br>
					<br>

					<div style="display: flex; width: 50%; margin: 0 auto;">
						<div style="width: 50%;">
							<form action="/MyWebSite/Login" method="get">
								<button class="btn btn-outline-secondary" type="submmit">戻る</button>
								<br>
							</form>
						</div>
						<div style="width: 50%;">
							<form action="/MyWebSite/SigninConfirm" method="post">
								<button class="btn btn-outline-secondary" type="submmit">登録確定！</button>
								<br>
							</form>
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
		</main>

	</div>

	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>