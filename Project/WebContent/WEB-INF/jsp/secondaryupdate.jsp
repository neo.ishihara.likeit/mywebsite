<%@page import="dao.PstDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="beans.*"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>

	<!-- 真ん中　-->
	<div id="bodder">

		<!-- メイン　-->
		<div id="middle">
			<!-- サイトマップ的な　-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="Index">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">リスト編集ページ</li>
				</ol>
			</nav>
			<!-- Main -->
			<main style="position: relative"> <!-- 削除ボタン -->
			<div class="datecorner">
				<form action="" SecondaryUpdate"" method="post">
					<input type="hidden" name="secondarydelele" value="true"> <input
						type="hidden" name="s_id" value="${secondary.id}"> <input
						type="hidden" name="primaryid" value="${secondary.primary_id}">
					<input type="submit" class="btn btn-danger" value="リストを削除する">
				</form>
			</div>
			<form action="SecondaryUpdate" method="post">
				<!-- タイトル -->
				<h1>
					<input type=text placeholder="リスト名" name="stitle"
						value="${secondary.title}" required>
				</h1>

				<!-- 表示順 -->
				<input type="number" placeholder="リストの表示順" min="1" max="2147483647"
					name="snumber" style="width: 800px; min-height: 50px;"
					value="${secondary.number}" required> <br> <input
					type="hidden" name="primaryid" value="${secondary.primary_id}">
				<input type="hidden" name="s_id" value="${secondary.id}"> <input
					type="submit" value="この内容でリストを更新する">
			</form>
			</main>
		</div>
	</div>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--
	        (ｰ`)
      ／⌒⌒⌒⌒⌒ヽ
    ／／￣￣￣フ ／
  ／（＿＿＿／ ／
（＿＿＿＿＿_ノ

-->