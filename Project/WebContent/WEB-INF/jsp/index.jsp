<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>


	<!-- 真ん中-->
	<div id="bodder">

		<!-- 左 -->
		<jsp:include page="/baselayout/left.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>

		<div id="middle">
			<!-- サイトマップ的な -->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">Home</li>
				</ol>
			</nav>
			<!-- Main -->
			<main>
			<div style="display: flex;">
			<c:forEach var="plist"
				items="${pst_plist}">
				<form action="Pst" method="get">
					<input type="hidden" value="${plist.id}" name="pst_id"> <input
						type="submit" class="btn-circle-3d" value="${plist.title}">
				</form>
			</c:forEach>
			</div>
			</main>
		</div>
		<!-- 右 -->
		<jsp:include page="/baselayout/right.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>
	</div>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>