<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<header>
		<div class="d-flex bd-highlight mb-3">
			<div class="p-2 bd-highlight">
				<a class="homebutton1 homebutton1-hover" href="Index">P</a><a
					class="homebutton1 homebutton1-hover" href="Index">r</a><a
					class="homebutton2 homebutton2-hover" href="Index">o</a><a
					class="homebutton1 homebutton1-hover" href="Index">r</a><a
					class="homebutton2 homebutton2-hover" href="Index">o</a><a
					class="homebutton1 homebutton1-hover" href="Index">a</a><a
					class="homebutton1 homebutton1-hover" href="Index">d</a>
			</div>
		</div>
	</header>

	<!-- 真ん中　-->
	<div id="bodder">

		<!-- Main -->
		<main style="position: relative"> <!-- タイトル -->
		<h1 style="text-align: center;">ユーザーページ</h1>
		<!-- メイン中身 --> <br>

		<div class="row" style="text-align: center;">
			<!-- Sign In -->
			<div style="width: 100%;">
				<div class="inputarea">
					<div style="display: flex; border-bottom: 1px solid black;">
						<div style="width: 33%;">
							<h4>権限</h4>
							<h2>${authorityname}</h2>
						</div>
						<div style="width: 33%;">
							<h4>ログインID</h4>
							<h2>${userdata.loginid}</h2>
						</div>
						<div style="width: 33%;">
							<h4>ユーザー作成日</h4>
							<h2>${userdata.createdate}</h2>
						</div>
					</div>
					<br>
					<form action="UserSetting" method="post">
						<div style="display: flex;">
							<div style="width: 50%;">
								<h4>アイコン</h4>
								<img src="img/icon/${icondata.URL}">
							</div>
							<div style="width: 50%;">
								<h4>名前</h4>
								<input type="text" name="name" value="${userdata.name}"
									autofocus>
							</div>
						</div>
						<br>
						<div style="display: flex;">
							<div style="width: 50%;">
								<h4>パスワード</h4>
								<input type="text" name="pass">
							</div>
							<div style="width: 50%;">
								<h4>パスワード再確認</h4>
								<input type="text" name="passkaku">
							</div>
						</div>

						<br>
						<br>
						<div style="display: flex; width: 50%; margin: 0 auto;">
							<div style="width: 50%;">
								<a class="btn btn-outline-secondary" href="User">戻る</a><br>
							</div>
							<div style="width: 50%;">
								<button class="btn btn-outline-secondary" type="submmit">変更確定！</button>
								<br>
								<br>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		</main>

	</div>

	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--       (ｰ`)
      ／⌒⌒⌒⌒⌒ヽ
    ／／￣￣￣フ ／
  ／（＿＿＿／ ／
（＿＿＿＿＿_ノ	-->