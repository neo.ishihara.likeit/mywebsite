<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
<!-- マークダウン -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
</head>
<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
	<!-- 真ん中 -->
	<div id="bodder">
		<!-- 真ん中 -->
		<div id="middle">
			<!-- サイトマップ的な　-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="/home.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">ページの編集</li>
				</ol>
			</nav>
			<!-- Main -->
			<main> <!-- 日付 -->
			<div class="datecorner">
				作成日:${pagedata.create_date }<br>
				<form action="PageUpdate" method="post">
					<input type="hidden" name="pagedelete" value="true"> <input
						type="hidden" name="page_id" value="${pagedata.id}"> <input
						type="submit" class="btn btn-danger" value="コースを削除する">
				</form>
			</div>
			<form action="PageUpdate" method="post">
				<!-- カテゴリー -->
				<div style="height: 50px; display: flex;">
					<div>
						カテゴリー：<select style="width: 100px" name="category" required>
							<c:forEach var="item" items="${calist}">
								<option value="${item.id}">${item.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<!-- タイトル -->
				<div>
					<h1>
						タイトル <input type="text" name="title" value="${pagedata.title}"maxlength="50"
							required>
					</h1>
				</div>
				<!-- メイン中身 -->
				<br>
				<div class="editor uk-container uk-margin-small">

					<textarea id="editor" name="markdown" rows="8" cols="40" required>${pagedata.textsub }</textarea>

					<button type="submit" onclick="draw_preview()"
						class="uk-button uk-button-default uk-margin-auto">この内容でページを更新する</button>

					<!-- HTMLに変換した結果 -->
					<div hidden class="col-md-12 ml-auto mr-auto text-left">
						<div class="preview" id="marked-preview"></div>
					</div>

					<!-- 変換結果コピペエリア -->
					<textarea hidden style="display: block;" onclick="this.select();"
						id="result" class="uk-textarea" name="result" rows="8" cols="100"
						readonly></textarea>
				</div>
				<input type="hidden" name="page_id" value="${pagedata.id}">
			</form>
			</main>
		</div>
	</div>
	<!-- マークダウンのスクリプト軍 -->
	<script
		src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/uikit.min.js"></script>
	<script>
		var simplemde = new SimpleMDE({
			element : document.getElementById("editor"),
			forceSync : true,
			spellChecker : false
		});
	</script>
	<script>
		marked.setOptions({
			sanitize : true,
			sanitizer : escape,
			breaks : true
		});

		function draw_preview() {
			var markdown = document.getElementById("editor").value;
			var html = marked(markdown);
			$('#marked-preview').html(html);
			$('#result').val(html);
		}
	</script>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>