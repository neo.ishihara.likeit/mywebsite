<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
<!-- マークダウン -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
</head>
<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
	<!-- 真ん中 -->
	<div id="bodder">
		<!-- 真ん中 -->
		<div id="middle">
			<!-- サイトマップ的な-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="Index">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">ForumUpdate</li>
				</ol>
			</nav>
			<!-- Main -->
			<main>
			<!-- 日付 -->
			${commentData.text }
			<div class="datecorner">
				作成日時：${commentData.create_date}<br>
					<form action="CommentCreate" method="post">
						<input type="hidden" name="comeback" value="true">
						<input type="submit" value="投稿を取り消す">
					</form>
			</div>
			<form action="CommentCreate" method="post">
				<h1>コメントへのコメントを投稿</h1>
				<!-- メイン中身 -->
				<!-- コメント投稿 -->
				<div class="comment_update">
					<!-- メイン中身 -->
					<div class="editor uk-container uk-margin-small">
						<textarea id="editor" name="markdown" rows="8" cols="40" required></textarea>
						<button type="submit" onclick="draw_preview()"
							class="uk-button uk-button-default uk-margin-auto">この内容でコメントを投稿する</button>
						<!-- HTMLに変換した結果 -->
						<div hidden class="col-md-12 ml-auto mr-auto text-left">
							<div class="preview" id="marked-preview"></div>
						</div>
						<!-- 変換結果コピペエリア -->
						<textarea hidden style="display: block;" onclick="this.select();"
							id="result" class="uk-textarea" name="result" rows="8" cols="100"
							readonly></textarea>
					</div>
					<input type="hidden" name="commentid" value="${commentData.id}">
					<input type="hidden" name="forum_id" value="${forumData.id}">
				</div>
			</form>
			</main>
		</div>
	</div>
	<!-- マークダウンのスクリプト軍 -->
	<script
		src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/uikit.min.js"></script>
	<script>
		var simplemde = new SimpleMDE({
			element : document.getElementById("editor"),
			forceSync : true,
			spellChecker : false
		});
	</script>
	<script>
		marked.setOptions({
			sanitize : true,
			sanitizer : escape,
			breaks : true
		});

		function draw_preview() {
			var markdown = document.getElementById("editor").value;
			var html = marked(markdown);
			$('#marked-preview').html(html);
			$('#result').val(html);
		}
	</script>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>