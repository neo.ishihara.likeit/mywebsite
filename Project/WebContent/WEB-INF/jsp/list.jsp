<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>


	<!-- 真ん中　-->
	<div id="bodder">


		<!-- 左メニュー -->
		<jsp:include page="/baselayout/left.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>


		<!-- 真ん中 -->
		<div id="middle">
			<!-- サイトマップ的な -->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item"><a href="Index">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">List</li>
				</ol>
			</nav>

			<!-- Main -->
			<main style="position: relative"> <!-- タイトル -->
			<div style="display: flex; color: #003ca8">
				<c:choose>
					<c:when test="${searchWord == '' && categorydata.id == null }">
						<h1>"全検索"</h1>
					</c:when>
					<c:when test="${searchWord == '' && categorydata.id != null }">
						<h1>"${categorydata.name }"</h1>
					</c:when>
					<c:when test="${searchWord != '' && categorydata.id == null }">
						<h1>"${searchWord }"</h1>
					</c:when>
					<c:otherwise>
						<h1>"${searchWord }" + "${categorydata.name }"</h1>
					</c:otherwise>
				</c:choose>
				<!-- 説明 -->
				<div>${itemCount}件</div>
			</div>

			<!-- メイン中身 -->
			<div>
				<ol class="searchlists">
					<c:forEach var="item" items="${itemList}">
						<c:if test="${item.page_id != ''}" var="flg" />
						<c:if test="${flg }">
							<!-- ページ -->
							<li class="sl">
								<div>
									<div class="sldate" style="display: flex;">
										<div style="width: 30%">${item.userdata.name}</div>
										<div style="width: 40%">カテゴリー:${item.categorydata.name }</div>
										<div style="width: 30%; text-align: right;">${item.create_date}</div>
									</div>
									<form action="Page" method="get">
										<h3>
											<button type="submit" class="tertiarybutton">${item.title }</button>
										</h3>
										<input type="hidden" name="page_id" value="${item.page_id }">
									</form>
								</div>
							</li>
						</c:if>
						<c:if test="${!flg }">
							<!-- 掲示板 -->
							<c:choose>
								<c:when test="${item.question == 1}">
									<li class="sl" style="background-color: #fff2f2;">
										<div>
											<div class="sldate" style="display: flex;">
												<div style="width: 30%">${item.userdata.name}</div>
												<div style="width: 40%">カテゴリー:${item.categorydata.name }</div>
												<div style="width: 30%; text-align: right;">${item.create_date}</div>
											</div>
											<div style="display: flex;">
												<div style="width: 90%;">
													<form action="Forum" method="get">
														<h3>
															<button type="submit" class="tertiarybutton">${item.title }</button>
														</h3>
														<input type="hidden" name="forum_id"
															value="${item.forum_id }">
													</form>
												</div>
												<div style="width: 10%;">
													<h3>質</h3>
												</div>
											</div>
										</div>
									</li>
								</c:when>
								<c:when test="${item.question == 2}">
									<li class="sl" style="background-color: #ddfde1;">
										<div>
											<div class="sldate" style="display: flex;">
												<div style="width: 30%">${item.userdata.name}</div>
												<div style="width: 40%">カテゴリー:${item.categorydata.name }</div>
												<div style="width: 30%; text-align: right;">${item.create_date}</div>
											</div>
											<div style="display: flex;">
												<div style="width: 90%;">
													<form action="Forum" method="get">
														<h3>
															<button type="submit" class="tertiarybutton">${item.title }</button>
														</h3>
														<input type="hidden" name="forum_id"
															value="${item.forum_id }">
													</form>
												</div>
												<div style="width: 10%;">
													<h3>解</h3>
												</div>
											</div>
										</div>
									</li>
								</c:when>
								<c:otherwise>
									<li class="sl" style="background-color: #ffffff;">
										<div>
											<div class="sldate" style="display: flex;">
												<div style="width: 30%">${item.userdata.name}</div>
												<div style="width: 40%">カテゴリー:${item.categorydata.name }</div>
												<div style="width: 30%; text-align: right;">${item.create_date}</div>
											</div>
											<div style="display: flex;">
												<div style="width: 90%;">
													<form action="Forum" method="get">
														<h3>
															<button type="submit" class="tertiarybutton">${item.title }</button>
														</h3>
														<input type="hidden" name="forum_id"
															value="${item.forum_id }">
													</form>
												</div>
											</div>
										</div>
									</li>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</ol>
			</div>
			<!-- ページネーション -->
			<ul class="pagination">
				<!-- 戻るボタン -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li><img
							src="img/toolicon/outline_keyboard_arrow_left_white_18dp.png"
							class="pagination-icon"></li>
					</c:when>
					<c:otherwise>
						<li><a
							href="List?search_word=${wearchWord }&page_num=${pageNum - 1}"><img
								src="img/toolicon/outline_keyboard_arrow_left_black_18dp.png"
								class="pagination-icon"></a></li>
					</c:otherwise>
				</c:choose>
				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum -5 : 1}"
					end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1"
					varStatus="status">
					<c:if test="${pageNum == status.index }" var="flg" />
					<c:if test="${flg }">
						<li>${status.index}</li>
					</c:if>
					<c:if test="${!flg }">
						<li><a
							href="List?search_word=${searchWord}&page_num=${status.index}">${status.index}</a></li>
					</c:if>
				</c:forEach>
				<!-- 進むボタン -->
				<c:choose>
					<c:when test="${pageNum == pageMax || pageMax == 0}">
						<li><img
							src="img/toolicon/outline_keyboard_arrow_right_white_18dp.png"
							class="pagination-icon"></li>
					</c:when>
					<c:otherwise>
						<li><a
							href="List?search_word=${wearchWord }&page_num=${pageNum + 1}"><img
								src="img/toolicon/outline_keyboard_arrow_right_black_18dp.png"
								class="pagination-icon"></a></li>
					</c:otherwise>
				</c:choose>
			</ul>
			</main>
		</div>
		<!-- 右 -->
		<jsp:include page="/baselayout/right.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>
	</div>

	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>