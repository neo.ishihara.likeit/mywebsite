<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>
<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>

	<!-- 真ん中 -->
	<div id="bodder">


		<!-- 左メニュー -->
		<jsp:include page="/baselayout/left.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>


		<!-- 真ん中 -->
		<div id="middle">
			<!-- サイトマップ的な -->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="Index">Home</a></li>
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="/page-languageJava.html">Java</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">Page</li>
				</ol>
			</nav>

			<!-- タグ カテゴリ -->
			<div class="ttagory">
				<div class="tcategorys">カテゴリー</div>
				<div class="tcategory">${categorydata.name }</div>
			</div>

			<!-- Main -->
			<c:if test="${notFound != null}" var="flg" /> <c:if
				test="${flg}">
				<main>
				<div>
					<h1>404</h1>
					<h1>フォーラムが見つかりません。</h1>
					<h2>削除、または移動された可能性があります。、</h2>
					<h2>正しいURLかご確認ください。</h2>
				</div>
				</main>
			</c:if>
			<c:if test="${!flg}">
				<main>
				<!-- 日付 -->
				<div class="datecorner">
					作成日:${pagedata.create_date}<br>
					<c:if
						test="${userdata.authority_method_id == 1 || pagedata.creater_id == userdata.id}">
						<div>
							<form action="PageUpdate" method="get">
								<input type="hidden" name="page_id" value="${pagedata.id}">
								<input type="submit" value="ページを編集">
							</form>
						</div>
					</c:if>
				</div>
				<!-- タイトル -->
				<h1>${pagedata.title}</h1>
				<!-- 中身 --> <div style="word-wrap: break-word;">${pagedata.text }</div>
				</main>
			<!-- 掲示板 -->
				<div class="page_forum">
					<c:forEach var="comment" items="${clist}" varStatus="loop">
						<div class="comment" style="position: relative">
							<div
								style="display: flex; padding-bottom: 10px; height: 60px; align-items: center;">
								<!--コメント番号-->
								<div style="position: absolute; top: 5px; left: 5px;">
									${loop.count }</div>
								<div style="width: 7%;">
									<span class="material-icons"><img
										src="img/icon/${comment.iconData.URL}"></span>
								</div>
								<div style="width: 53%;">${comment.userData.name}</div>
								<div style="width: 20%;">ID:${comment.userData.loginid}</div>
								<div style="width: 20%; text-align: right;">${comment.create_date}
									<c:if
										test="${userdata.authority_method_id == 1 || comment.creater_id == userdata.id}">
										<div>
											<form action="CommentUpdate" method="get">
												<input type="hidden" name="comment_id" value="${comment.id}">
												<input type="hidden" name="forum_id" value="${forumData.id}">
												<input type="submit" value="コメントを編集">
											</form>
										</div>
									</c:if>
									<c:if test="${userdata != null}">
										<form action="CommentCreate" method="get">
											<input type="hidden" name="comment_id" value="${comment.id}">
											<input type="hidden" name=forum_id value="${forumData.id}">
											<input type="submit" value="この投稿に対してコメントをする">
										</form>
									</c:if>
								</div>
							</div>
							<div style="word-wrap: break-word;">${comment.text}</div>
						</div>
					</c:forEach>

					<!-- セッションの有無でログイン、ユーザー表示切替 -->
					<c:if test="${userdata == null}" var="flg" />
					<c:if test="${flg}">
						<!-- コメント投稿 -->
						<div class="comment_update">
							<h5>ログインをするとコメント投稿が出来るようになります</h5>
							<form class="form-inline" action="/MyWebSite/Login" method="get">
								<button class="btn btn-outline-info" type="submit">ログイン・サインイン</button>
							</form>
						</div>
					</c:if>
					<c:if test="${!flg}">
						<!-- コメント投稿 -->
						<div class="comment_update">
							<h5>コメント投稿</h5>
							<!-- メイン中身 -->
							<form action="Page" method="post">
								<div class="editor uk-container uk-margin-small">

									<textarea id="editor" name="markdown" rows="8" cols="40"
										required></textarea>

									<button type="submit" onclick="draw_preview()"
										class="uk-button uk-button-default uk-margin-auto">この内容でコメントを投稿する</button>

									<!-- HTMLに変換した結果 -->
									<div hidden class="col-md-12 ml-auto mr-auto text-left">
										<div class="preview" id="marked-preview"></div>
									</div>

									<!-- 変換結果コピペエリア -->
									<textarea hidden style="display: block;"
										onclick="this.select();" id="result" class="uk-textarea"
										name="result" rows="8" cols="100" readonly></textarea>
								</div>
								<input type="hidden" name="page_id" value="${pagedata.id}">
								<input type="hidden" name="commentup" value="true">
							</form>
						</div>
					</c:if>
				</div>
			</c:if>
		</div>
		<!-- 右 -->
		<jsp:include page="/baselayout/right.jsp">
			<jsp:param name="flg" value="${flg}" />
		</jsp:include>
	</div>
	<!-- マークダウンのスクリプト軍 -->
	<script
		src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/uikit.min.js"></script>
	<script>
		var simplemde = new SimpleMDE({
			element : document.getElementById("editor"),
			forceSync : true,
			spellChecker : false
		});
	</script>
	<script>
		marked.setOptions({
			sanitize : true,
			sanitizer : escape,
			breaks : true
		});

		function draw_preview() {
			var markdown = document.getElementById("editor").value;
			var html = marked(markdown);
			$('#marked-preview').html(html);

			$('#result').val(html);
		}
	</script>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>