<%@page import="dao.PstDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="beans.*"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Proroad</title>
<jsp:include page="/baselayout/head.html" />
</head>

<body style="background-color: azure">
	<!-- ヘッダー -->
	<jsp:include page="/baselayout/header.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>

	<!-- 真ん中　-->
	<div id="bodder">

		<!-- メイン　-->
		<div id="middle">
			<!-- サイトマップ的な　-->
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb" style="">
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="Index">Home</a></li>
					<li class="breadcrumb-item" style="list-style: none;"><a
						href="SecondaryList">${pst_p.title}</a></li>
					<li class="breadcrumb-item active" aria-current="page"
						style="list-style: none;">コース編集画面</li>
				</ol>
			</nav>
			<!-- Main -->
			<main style="position: relative"> <!-- 日付 -->
			<div class="datecorner">${pst_p.create_date}<br>
				<div>
					<form action="PrimaryUpdate" method="get">
						<input type="hidden" name="primaryid" value="${pst_p.id}">
						<input type="submit" value="コース名、説明を編集する">
					</form>
				</div>
				<div>
					<form action="SecondaryCreate" method="get">
						<input type="hidden" name="primaryid" value="${pst_p.id}">
						<input type="submit" value="リストの追加">
					</form>
				</div>
			</div>
			<!-- タイトル -->
			<h1>${pst_p.title}</h1>

			<!-- 説明 -->
			<p>${pst_p.detail}</p>

			<!-- メイン中身 -->

			<div class="pstbox">
				<c:forEach var="s" items="${pst_s}">
					<div class="selectionbox">
						<div style="border-bottom: 1px solid black">
							<h3>${s.number}.${s.title}</h3>
							<form action="SecondaryUpdate" method="get">
								<input type="hidden" name="sid" value="${s.id}"> <input
									type="submit" value="リストの編集">
							</form>
							<form action="TertiaryCreate" method="get">
								<input type="hidden" name="sid" value="${s.id}"> <input
									type="hidden" name="primaryid" value="${pst_p.id}"> <input
									type="submit" value="URLの追加">
							</form>
						</div>
						<c:forEach var="t" items="${pst_t}">
							<c:if test="${s.id == t.secondary_id}">
								<div class="tertiarylist">
									<div class="tertiary">
										<a href="page.html" class="text-decoration-none">${t.number}.${t.title}</a>
									</div>
									<div class="tertiarybutton">
										<form action="TertiaryUpdate" method="get">
											<input type="hidden" name="sid" value="${s.id}"> <input
												type="hidden" name="primaryid" value="${pst_p.id}">
											<input type="hidden" name="tid" value="${t.id}"> <input
												type="submit" value="URLの変更">
										</form>
									</div>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</c:forEach>
			</div>
			</main>
		</div>
	</div>
	<!-- フッター -->
	<jsp:include page="/baselayout/footer.jsp">
		<jsp:param name="userdate" value="${userdata}" />
		<jsp:param name="flg" value="${flg}" />
	</jsp:include>
</body>
</html>
<!--
	        (ｰ`)
      ／⌒⌒⌒⌒⌒ヽ
    ／／￣￣￣フ ／
  ／（＿＿＿／ ／
（＿＿＿＿＿_ノ

-->