package proroad;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CommentDataBeans;
import beans.ForumBeans;
import beans.UserDataBeans;
import dao.CommentDAO;
import dao.ForumDAO;

/**
 * Servlet implementation class CommentUpdate
 */
@WebServlet("/CommentCreate")
public class CommentCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ユーザーデータがない場合はIndexへ飛ばす
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		ForumBeans forum = ForumDAO.getForum(Integer.parseInt(request.getParameter("forum_id")));
		CommentDataBeans comment = CommentDAO.getComment(Integer.parseInt(request.getParameter("comment_id")));
		comment.setTextsub(helper.TextChange.changeText(comment.getTextsub()));
		request.setAttribute("commentData", comment);
		request.setAttribute("forumData", forum);
		request.getRequestDispatcher("/WEB-INF/jsp/commentcreate.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ユーザーデータがない場合はIndexへ飛ばす
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("userdata");
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		CommentDataBeans comment = CommentDAO.getComment(Integer.parseInt(request.getParameter("commentid")));
		CommentDataBeans com = new CommentDataBeans();
		com.setText(request.getParameter("result"));
		com.setTextsub(request.getParameter("markdown"));
		com.setCreater_id(user.getId());
		com.setComment_id(comment.getId());
		CommentDAO.setComment(com);
		if(Objects.nonNull(session.getAttribute("pageid"))) {
			request.setAttribute("page_id", String.valueOf(session.getAttribute("pageid")));
			request.getRequestDispatcher("Page").forward(request, response);
			return;
		}
		else if(Objects.nonNull(request.getParameter("forum_id"))){
			request.setAttribute("forum_id", request.getParameter("forum_id"));
			request.getRequestDispatcher("Forum").forward(request, response);
			return;
		}else {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
	}
}
