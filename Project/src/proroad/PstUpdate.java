package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Pst_PrimaryBeans;
import beans.Pst_SecondaryBeans;
import beans.Pst_TertiaryBeans;

/**
 * Servlet implementation class SecondaryList
 */
@WebServlet("/PstUpdate")
public class PstUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PstUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ユーザーデータがない場合はIndexへ飛ばす 法外には法外だ。
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}


		if(dao.PstDAO.getPrimarydata(Integer.parseInt(request.getParameter("primaryid"))) == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		Pst_PrimaryBeans pst_p = dao.PstDAO.getPrimarydata(Integer.parseInt(request.getParameter("primaryid")));
		ArrayList<Pst_SecondaryBeans> pst_s = dao.PstDAO.getAllSecondarydeta(pst_p.getId());
		ArrayList<Pst_TertiaryBeans> pst_t = dao.PstDAO.getAllTertiary();

		request.setAttribute("pst_p", pst_p);
		request.setAttribute("pst_s", pst_s);
		request.setAttribute("pst_t", pst_t);

		request.getRequestDispatcher("/WEB-INF/jsp/pstupdate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
	}

}
