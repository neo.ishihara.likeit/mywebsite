package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Pst_PrimaryBeans;
import beans.UserDataBeans;

/**
 * Servlet implementation class PrimaryCreate
 */
@WebServlet("/PrimaryCreate")
public class PrimaryCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrimaryCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションを持たないで遷移した場合インデックス画面に飛ばす。
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null)
		{
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}

		request.getRequestDispatcher("/WEB-INF/jsp/primarycreate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		Pst_PrimaryBeans primary = new Pst_PrimaryBeans();
		primary.setTitle(request.getParameter("ptitle"));
		primary.setDetail(request.getParameter("pdetail"));
		UserDataBeans user = (UserDataBeans) session.getAttribute("userdata");
		primary.setCreater_id(user.getId());

		dao.PstDAO.setPrimarydata(primary);
		request.getRequestDispatcher("Index").forward(request, response);
		return;
	}

}
