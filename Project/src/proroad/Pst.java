package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryBeans;
import beans.Pst_PrimaryBeans;
import beans.Pst_SecondaryBeans;
import beans.Pst_TertiaryBeans;
import dao.CategoryDAO;

/**
 * Servlet implementation class Pst
 */
@WebServlet("/Pst")
public class Pst extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pst() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		if(dao.PstDAO.getPrimarydata(Integer.parseInt(request.getParameter("pst_id"))) == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		Pst_PrimaryBeans pst_p = dao.PstDAO.getPrimarydata(Integer.parseInt(request.getParameter("pst_id")));
		ArrayList<Pst_SecondaryBeans> pst_s = dao.PstDAO.getAllSecondarydeta(pst_p.getId());
		ArrayList<Pst_TertiaryBeans> pst_t = dao.PstDAO.getAllTertiary();

		request.setAttribute("pst_p", pst_p);
		request.setAttribute("pst_s", pst_s);
		request.setAttribute("pst_t", pst_t);


		//カテゴリー読み込み
				ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
				request.setAttribute("calist", calist);
		request.getRequestDispatcher("/WEB-INF/jsp/pst.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
