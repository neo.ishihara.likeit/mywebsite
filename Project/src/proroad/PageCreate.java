package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ForumBeans;
import beans.PageDataBeans;
import dao.CategoryDAO;
import dao.ForumDAO;
import dao.PageDAO;

/**
 * Servlet implementation class Page
 */
@WebServlet("/PageCreate")
public class PageCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PageCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションを持たないで遷移した場合インデックス画面に飛ばす。
				HttpSession session = request.getSession();
				if(session.getAttribute("userdata") == null)
				{
					request.getRequestDispatcher("Index").forward(request, response);
					return;
				}
				//カテゴリー読み込み
				ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
				request.setAttribute("calist", calist);
		request.getRequestDispatcher("/WEB-INF/jsp/pagecreate.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ユーザーデータがない場合はIndexへ飛ばす 法外には法外だ。
				HttpSession session = request.getSession();
				if(session.getAttribute("userdata") == null) {
					request.getRequestDispatcher("Index").forward(request, response);
					return;
				}

		request.setCharacterEncoding("UTF-8");
		PageDataBeans page = new PageDataBeans();
		page.setCategory_id(Integer.parseInt(request.getParameter("category")));
		page.setTitle(request.getParameter("title"));
		page.setText(request.getParameter("result"));
		page.setTextsub(request.getParameter("markdown"));
		page.setCreater_id(Integer.parseInt(request.getParameter("creater")));
		PageDAO.setPage(page);

		ForumBeans forum = new ForumBeans();
		page = PageDAO.getLastPage();
		forum.setTitle(request.getParameter("title"));
		forum.setPage_id(page.getId());
		forum.setCategory_id(Integer.parseInt(request.getParameter("category")));
		forum.setCreater_id(Integer.parseInt(request.getParameter("creater")));
		ForumDAO.setForum(forum);
		session.removeAttribute("pageid");
		session.setAttribute("pageid", page.getId());
		request.getRequestDispatcher("Page").forward(request, response);
		return;
	}

}
