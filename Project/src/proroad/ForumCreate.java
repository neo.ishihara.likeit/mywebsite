package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.CommentDataBeans;
import beans.ForumBeans;
import dao.CategoryDAO;
import dao.CommentDAO;
import dao.ForumDAO;

/**
 * Servlet implementation class Page
 */
@WebServlet("/ForumCreate")
public class ForumCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ForumCreate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションを持たないで遷移した場合インデックス画面に飛ばす。
		HttpSession session = request.getSession();
		if (session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		//カテゴリー読み込み
				ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
				request.setAttribute("calist", calist);
		request.getRequestDispatcher("/WEB-INF/jsp/forumcreate.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ユーザーデータがない場合はIndexへ飛ばす 外法には外法だ。
		HttpSession session = request.getSession();
		if (session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}

		request.setCharacterEncoding("UTF-8");
		ForumBeans forum = new ForumBeans();
		CommentDataBeans comment = new CommentDataBeans();
		forum.setTitle(request.getParameter("title"));
		if(request.getParameter("question") != null)
		forum.setQuestion(Integer.parseInt(request.getParameter("question")));
		else
		forum.setQuestion(0);
		forum.setCategory_id(Integer.parseInt(request.getParameter("category")));
		forum.setCreater_id(Integer.parseInt(request.getParameter("creater")));
		ForumDAO.setForum(forum);
		forum = ForumDAO.getLastForum();
		comment.setForum_id(forum.getId());
		comment.setCreater_id(Integer.parseInt(request.getParameter("creater")));
		comment.setText(request.getParameter("result"));
		comment.setTextsub(request.getParameter("markdown"));
		CommentDAO.setComment(comment);
		request.setAttribute("forum_id", new Integer(forum.getId()));
		request.setAttribute("aaa", "aaa");
		request.getRequestDispatcher("Forum").forward(request, response);
		request.setAttribute("bbb", "bbb");
		return;
	}

}
