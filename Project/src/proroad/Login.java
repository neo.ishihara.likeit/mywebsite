package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.IconDataBeans;
import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		//ログイン処理
		if(request.getParameter("login") != null) {
			//チェック
			if(request.getParameter("loginid") == "" || request.getParameter("pass") == "")
			{
				request.setAttribute("lerror", "1");
				doGet(request, response);
				return;
			}else {
			//成功
			UserDataBeans user = new UserDataBeans();
			user = UserDAO.login(request.getParameter("loginid"),request.getParameter("pass"));
			if(user == null)
			{
				request.setAttribute("nouser", "1");
				doGet(request, response);
			}
			IconDataBeans icon = UserDAO.getIconData(user.getIcon_method_id());
			session.setAttribute("icondata", icon);
			session.setAttribute("userdata", user);
			request.getRequestDispatcher("Index").forward(request, response);
			return;
			}
		}

		doGet(request,response);
	}

}
