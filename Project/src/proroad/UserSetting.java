package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserSetting
 */
@WebServlet("/UserSetting")
public class UserSetting extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSetting() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//セッションを持たないで遷移した場合ホームに飛ばす
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		if(session.getAttribute("userdata") == null)
		{
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		UserDataBeans user = (UserDataBeans) session.getAttribute("userdata");
		request.setAttribute("authorityname", UserDAO.getAuthorityName(user.getAuthority_method_id()));

		request.getRequestDispatcher("WEB-INF/jsp/usersetting.jsp").forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");
		//受け取ったデータをアップデートして受け取ったデータをセッションに戻す	ねお＠Neonald.da.vinti
		UserDataBeans user = new UserDataBeans();

		//パスワードが一緒かどうか確認
		if(!(request.getParameter("pass").equals(request.getParameter("passkaku")))) {
			request.getRequestDispatcher("User").forward(request, response);
			return;
		}

		user = (UserDataBeans) session.getAttribute("userdata");

		if(request.getParameter("pass").equals(""))
			user.setPassword(null);
		else
			user.setPassword(request.getParameter("pass"));


		user.setName(request.getParameter("name"));
		user.setIcon_method_id(1);

		user = UserDAO.updateuser(user);

		session.removeAttribute("userdata");
		session.setAttribute("userdata", user);
		request.getRequestDispatcher("WEB-INF/jsp/user.jsp").forward(request, response);
	}

}
