package proroad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.CommentDataBeans;
import beans.ForumBeans;
import beans.PageDataBeans;
import beans.UserDataBeans;
import dao.CategoryDAO;
import dao.CommentDAO;
import dao.ForumDAO;
import dao.PageDAO;

/**
 * Servlet implementation class Page
 */
@WebServlet("/Page")
public class Page extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Page() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		PageDataBeans page = null;
		if(request.getParameter("page_id") != null) {
			page = PageDAO.getPage(Integer.parseInt(request.getParameter("page_id")));}
		else if(Objects.nonNull(session.getAttribute("pageid"))) {
			page = PageDAO.getPage(Integer.parseInt(String.valueOf(session.getAttribute("pageid"))));
			session.removeAttribute("pageid");
			}
		else {
			page = PageDAO.getLastPage();}
		//404
		if (page.getTitle() == null) {
			request.setAttribute("notFound", "404");}
		//フォーラム、コメントの取得
		ForumBeans forum = ForumDAO.getPage_id2Forum(page.getId());
		ArrayList<CommentDataBeans> clist = CommentDAO.getForum2Comment(forum.getId());
		int aa = new Integer(page.getId());
		session.setAttribute("pageid", aa);
		request.setAttribute("pagedata", page);
		request.setAttribute("clist", clist);
		request.setAttribute("forumData", forum);
		//ページのカテゴリーを取得
				CategoryBeans category = CategoryDAO.getCategory(page.getCategory_id());
				request.setAttribute("categorydata", category);

		//カテゴリー読み込み
		ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
		request.setAttribute("calist", calist);
		request.getRequestDispatcher("/WEB-INF/jsp/page.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		if (request.getParameter("commentup") != null) {
			ForumBeans forum = ForumDAO.getPage_id2Forum(Integer.parseInt(request.getParameter("page_id")));
			CommentDataBeans comment = new CommentDataBeans();
			UserDataBeans user = (UserDataBeans) session.getAttribute("userdata");
			comment.setCreater_id(user.getId());
			comment.setText(request.getParameter("result"));
			comment.setTextsub(request.getParameter("markdown"));
			comment.setForum_id(forum.getId());
			CommentDAO.setComment(comment);
		}
		doGet(request,response);
	}

}
