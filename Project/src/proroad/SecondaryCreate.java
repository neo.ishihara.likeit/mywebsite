package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Pst_SecondaryBeans;
import dao.PstDAO;

/**
 * Servlet implementation class PrimaryCreate
 */
@WebServlet("/SecondaryCreate")
public class SecondaryCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SecondaryCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションを持たないで遷移した場合インデックス画面に飛ばす。
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null)
		{
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		request.setAttribute("primaryid", request.getParameter("primaryid"));
		request.getRequestDispatcher("/WEB-INF/jsp/secondarycreate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		Pst_SecondaryBeans secondary = new Pst_SecondaryBeans();
		secondary.setTitle(request.getParameter("stitle"));
		secondary.setNumber(Integer.parseInt(request.getParameter("snuber")));
		secondary.setPrimary_id(Integer.parseInt(request.getParameter("primaryid")));

		PstDAO.setSecondarydata(secondary);
		request.setAttribute("primaryid", request.getParameter("primaryid"));
		request.getRequestDispatcher("PstUpdate").forward(request, response);
		return;
	}

}
