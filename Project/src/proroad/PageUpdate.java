package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ForumBeans;
import beans.PageDataBeans;
import dao.CategoryDAO;
import dao.ForumDAO;
import dao.PageDAO;

/**
 * Servlet implementation class Page
 */
@WebServlet("/PageUpdate")
public class PageUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PageUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ユーザーデータがない場合はIndexへ飛ばす 法外には法外だ。
				HttpSession session = request.getSession();
				if(session.getAttribute("userdata") == null) {
					request.getRequestDispatcher("Index").forward(request, response);
					return;
				}
				//カテゴリー読み込み
				ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
				request.setAttribute("calist", calist);
		PageDataBeans page = PageDAO.getPage(Integer.parseInt(request.getParameter("page_id")));
		request.setAttribute("pagedata", page);
		request.getRequestDispatcher("/WEB-INF/jsp/pageupdate.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		//ユーザーデータがない場合はIndexへ飛ばす
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		PageDataBeans page = new PageDataBeans();
		page.setId(Integer.parseInt(request.getParameter("page_id")));

		if(request.getParameter("pagedelete") != null) {
			PageDAO.deletePage(page);
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		page.setCategory_id(Integer.parseInt(request.getParameter("category")));
		page.setTitle(request.getParameter("title"));
		page.setText(request.getParameter("result"));
		page.setTextsub(request.getParameter("markdown"));
		PageDAO.updatePage(page);

		ForumBeans forum = ForumDAO.getPage_id2Forum(Integer.parseInt(request.getParameter("page_id")));
		forum.setTitle(request.getParameter("title"));
		forum.setCategory_id(Integer.parseInt(request.getParameter("category")));
		ForumDAO.updateForum(forum);

		request.getRequestDispatcher("Page").forward(request, response);
		return;
	}

}
