package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Pst_PrimaryBeans;
import dao.PstDAO;

/**
 * Servlet implementation class PrimaryCreate
 */
@WebServlet("/PrimaryUpdate")
public class PrimaryUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrimaryUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Pst_PrimaryBeans pst_p = PstDAO.getPrimarydata(Integer.parseInt(request.getParameter("primaryid")));
		request.setAttribute("primarydata", pst_p);

		request.getRequestDispatcher("/WEB-INF/jsp/primaryupdate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		if(request.getParameter("primarydelete") != null) {
			Pst_PrimaryBeans pst_p = new Pst_PrimaryBeans();
			pst_p.setId(Integer.parseInt(request.getParameter("primaryid")));
			PstDAO.deletePrimarydata(pst_p);
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}


		Pst_PrimaryBeans primary = new Pst_PrimaryBeans();
		primary.setTitle(request.getParameter("ptitle"));
		primary.setDetail(request.getParameter("pdetail"));
		primary.setId(Integer.parseInt(request.getParameter("primaryid")));

		PstDAO.updatePrimarydata(primary);

		request.setAttribute("primaryid",request.getParameter("primaryid"));
		request.getRequestDispatcher("PstUpdate").forward(request, response);
		return;
	}

}
