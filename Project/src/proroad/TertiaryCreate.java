package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Pst_TertiaryBeans;
import dao.PstDAO;

/**
 * Servlet implementation class PrimaryCreate
 */
@WebServlet("/TertiaryCreate")
public class TertiaryCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TertiaryCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("primaryid", request.getParameter("primaryid"));
		request.setAttribute("sid", request.getParameter("sid"));
		request.getRequestDispatcher("/WEB-INF/jsp/tertiarycreate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		Pst_TertiaryBeans tertiary = new Pst_TertiaryBeans();
		tertiary.setSecondary_id(Integer.parseInt(request.getParameter("s_id")));
		tertiary.setTitle(request.getParameter("ttitle"));
		tertiary.setNumber(Integer.parseInt(request.getParameter("tnumber")));
		tertiary.setPage_id(Integer.parseInt(request.getParameter("tpage_id")));

		PstDAO.setTertiarydata(tertiary);
		request.setAttribute("primaryid", request.getParameter("primaryid"));
		request.getRequestDispatcher("PstUpdate").forward(request, response);
		return;
	}

}
