package proroad;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.IconDataBeans;
import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class SigninConfirm
 */
@WebServlet("/SigninConfirm")
public class SigninConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SigninConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		request.setAttribute("sloginid", request.getParameter("sloginid"));
			//チェック
			if(request.getParameter("sloginid") == "" || request.getParameter("sname") == "" ||
					request.getParameter("spass") == "" || request.getParameter("spasskaku") == "" ||
							(!(request.getParameter("spass").equals(request.getParameter("spasskaku")))))
			{
				request.setAttribute("serror", "sainnera-");
				request.getRequestDispatcher("Login").forward(request, response);
				return;
			}else if (UserDAO.checkloginid(request.getParameter("sloginid"))){
				request.setAttribute("kaburi", "kaburi");
				request.getRequestDispatcher("Login").forward(request, response);
				return;
			}else {
			//処理
			UserDataBeans user = new UserDataBeans();
			Date sqlNow = new Date(System.currentTimeMillis());
			java.util.Date utilDate = sqlNow;
			Date sqlDate = new Date(utilDate.getTime());

			user.setLoginid(request.getParameter("sloginid"));
			user.setName(request.getParameter("sname"));
			user.setCreatedate(sqlDate);
			user.setPassword(request.getParameter("spass"));
			user.setAuthority_method_id(2);
			user.setIcon_method_id(1);

			request.setAttribute("authority", UserDAO.getAuthorityName(user.getAuthority_method_id()));
			session.setAttribute("signuser", user);
			}

		request.getRequestDispatcher("/WEB-INF/jsp/signinconfirm.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();



		//サインイン処理
		if(UserDAO.signinUser((UserDataBeans) session.getAttribute("signuser")))
		{
			UserDataBeans loginuser = new UserDataBeans();
			UserDataBeans user = new UserDataBeans();
			IconDataBeans sicon = new IconDataBeans();

			loginuser = (UserDataBeans) session.getAttribute("signuser");
			user = UserDAO.login(loginuser.getLoginid(), loginuser.getPassword());
			user.setPassword(user.getPassword());
			sicon = UserDAO.getIconData(user.getIcon_method_id());

			session.setAttribute("icondata", sicon);
			session.setAttribute("userdata",user);

			request.getRequestDispatcher("Index").forward(request, response);
		}else {
			request.setAttribute("sippai", "1");
			doGet(request, response);
		}


	}

}
