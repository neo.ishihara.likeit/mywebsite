package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryBeans;
import beans.CommentDataBeans;
import beans.ForumBeans;
import dao.CategoryDAO;
import dao.CommentDAO;
import dao.ForumDAO;

/**
 * Servlet implementation class Forum
 */
@WebServlet("/Forum")
public class Forum extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Forum() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ForumBeans forum = new ForumBeans();
		if (request.getAttribute("forum_id") != null) {
			if (!(request.getAttribute("forum_id").equals("")))
				forum = ForumDAO.getForum(Integer.parseInt(String.valueOf(request.getAttribute("forum_id"))));
		} else if (request.getParameter("forum_id") != null) {
			forum = ForumDAO.getForum(Integer.parseInt(request.getParameter("forum_id")));
		} else
			forum = ForumDAO.getLastForum();
		//フォーラムのIDが正しいかチェック
		if (forum.getTitle() == null)
			request.setAttribute("notFound", "404");
		ArrayList<CommentDataBeans> clist = CommentDAO.getForum2Comment(forum.getId());
		request.setAttribute("forumdata", forum);
		request.setAttribute("clist", clist);
		//フォーラムのカテゴリーを取得
		CategoryBeans category = CategoryDAO.getCategory(forum.getCategory_id());
		request.setAttribute("categorydata", category);

		//全カテゴリー読み込み
		ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
		request.setAttribute("calist", calist);
		request.getRequestDispatcher("/WEB-INF/jsp/forum.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// TODO Auto-generated method stub
		if (request.getParameter("commentup") != null) {
			CommentDataBeans comment = new CommentDataBeans();
			comment.setCreater_id(Integer.parseInt(request.getParameter("user_id")));
			comment.setText(request.getParameter("result"));
			comment.setTextsub(request.getParameter("markdown"));
			comment.setForum_id(Integer.parseInt(String.valueOf(request.getParameter("forum_id"))));
			CommentDAO.setComment(comment);
			request.setAttribute("forum_id", Integer.parseInt(request.getParameter("forum_id")));
		}
		doGet(request, response);
	}

}
