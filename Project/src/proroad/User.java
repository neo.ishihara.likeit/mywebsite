package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class User
 */
@WebServlet("/User")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//セッションを持たないで遷移した場合ログイン画面に飛ばす。
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null)
		{
			request.getRequestDispatcher("Login").forward(request, response);
			return;
		}
		UserDataBeans user = (UserDataBeans) session.getAttribute("userdata");
		request.setAttribute("authorityname", UserDAO.getAuthorityName(user.getAuthority_method_id()));
		request.getRequestDispatcher("WEB-INF/jsp/user.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		session.removeAttribute("userdata");
		session.removeAttribute("icondata");
		request.getRequestDispatcher("Index").forward(request, response);
	}

}
