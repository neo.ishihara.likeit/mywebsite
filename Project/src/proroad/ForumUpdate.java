package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.CommentDataBeans;
import beans.ForumBeans;
import dao.CategoryDAO;
import dao.CommentDAO;
import dao.ForumDAO;

/**
 * Servlet implementation class Page
 */
@WebServlet("/ForumUpdate")
public class ForumUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ForumUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションを持たないで遷移した場合インデックス画面に飛ばす。
		HttpSession session = request.getSession();
		if (session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		ForumBeans forum = ForumDAO.getForum(Integer.parseInt(request.getParameter("forum_id")));
		request.setAttribute("forumdata", forum);
		CommentDataBeans comment = CommentDAO.getForum1Comment(forum.getId());
		request.setAttribute("commentdata", comment);
		//カテゴリー読み込み
				ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
				request.setAttribute("calist", calist);
		request.getRequestDispatcher("/WEB-INF/jsp/forumupdate.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		//ユーザーデータがない場合はIndexへ飛ばす 外法には外法だ。
		HttpSession session = request.getSession();
		if (session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		if(request.getParameter("forumdelete") != null) {
			ForumDAO.deleteForum(Integer.parseInt(request.getParameter("forum_id")));
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		ForumBeans forum = new ForumBeans();
		forum.setId(Integer.parseInt(request.getParameter("forum_id")));
		forum.setTitle(request.getParameter("title"));
		forum.setQuestion(Integer.valueOf(request.getParameter("question")));
		forum.setCategory_id(Integer.parseInt(request.getParameter("category")));
		ForumDAO.updateForum(forum);
		CommentDataBeans comment = CommentDAO.getForum1Comment(forum.getId());
		comment.setText(request.getParameter("result"));
		comment.setTextsub(request.getParameter("markdown"));
		CommentDAO.updateComment(comment);
		request.setAttribute("forum_id", forum.getId());
		request.getRequestDispatcher("Forum").forward(request, response);
		return;
	}

}
