package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Pst_TertiaryBeans;
import dao.PstDAO;

/**
 * Servlet implementation class PrimaryCreate
 */
@WebServlet("/TertiaryUpdate")
public class TertiaryUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TertiaryUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ユーザーデータがない場合はIndexへ飛ばす 法外には法外だ。
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}

		Pst_TertiaryBeans pst_t = PstDAO.getTertiarydata(Integer.parseInt(request.getParameter("tid")));
		request.setAttribute("tertiary", pst_t);
        request.setAttribute("primaryid", request.getParameter("primaryid"));
		request.getRequestDispatcher("/WEB-INF/jsp/tertiaryupdate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		if(request.getParameter("tertiarydelele") != null) {
			Pst_TertiaryBeans pst_t = new Pst_TertiaryBeans();
			pst_t.setId(Integer.parseInt(request.getParameter("t_id")));
			PstDAO.deleteTertiarydata(pst_t);
			request.setAttribute("primaryid", request.getParameter("primaryid"));
			request.getRequestDispatcher("PstUpdate").forward(request, response);
			return;
		}


		Pst_TertiaryBeans secondary = new Pst_TertiaryBeans();
		secondary.setId(Integer.parseInt(request.getParameter("t_id")));
		secondary.setTitle(request.getParameter("ttitle"));
		secondary.setNumber(Integer.parseInt(request.getParameter("tnumber")));
		secondary.setPage_id(Integer.parseInt(request.getParameter("tpage_id")));

		PstDAO.updateTertiarydata(secondary);
		secondary = PstDAO.getTertiarydata(Integer.parseInt(request.getParameter("t_id")));
		request.setAttribute("primaryid", request.getParameter("primaryid"));
		request.getRequestDispatcher("PstUpdate").forward(request, response);
		return;
	}

}
