package proroad;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ListBeans;
import dao.CategoryDAO;
import dao.ListDAO;

/**
 * Servlet implementation class List
 */
@WebServlet("/List")
public class List extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示するリストの数
	private static int LIST_MAX_ITEM_COUNT = 20;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public List() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String searchWord = (request.getParameter("search_word") == null ? "" : request.getParameter("search_word"));
		//表示ページ番号 未指定の場合は1ページ目を表示
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
		//検索ワードをセッションに保存
		session.setAttribute("searchWord", searchWord);

		//カテゴリーの選択
		int category_id = 0;
		if(request.getParameter("category") != null)
			category_id = Integer.parseInt(request.getParameter("category"));
		//商品リストを取得 ページ表示文のみ
		ArrayList<ListBeans> ResultList = ListDAO.getListByName(searchWord, pageNum, LIST_MAX_ITEM_COUNT,category_id);

		//検索ワードに対しての総ページ数を取得
		double itemCount = ListDAO.getListCount(searchWord,category_id);
		int pageMax = (int)Math.ceil(itemCount / LIST_MAX_ITEM_COUNT);

		//総アイテム数
		request.setAttribute("itemCount", (int)itemCount);
		//総ページ数
		request.setAttribute("pageMax",pageMax);
		//表示ページ
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("itemList", ResultList);
		//検索されたカテゴリー情報
		if(request.getParameter("category") != null) {
			CategoryBeans cate = CategoryDAO.getCategory(Integer.parseInt(request.getParameter("category")));
			request.setAttribute("categorydata", cate);
		}

		//カテゴリー読み込み
		ArrayList<CategoryBeans> calist = CategoryDAO.getCategory();
		request.setAttribute("calist", calist);
		request.getRequestDispatcher("/WEB-INF/jsp/list.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
