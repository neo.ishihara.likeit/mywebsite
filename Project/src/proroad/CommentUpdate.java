package proroad;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CommentDataBeans;
import dao.CommentDAO;

/**
 * Servlet implementation class CommentUpdate
 */
@WebServlet("/CommentUpdate")
public class CommentUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ユーザーデータがない場合はIndexへ飛ばす
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}

		CommentDataBeans comment = CommentDAO.getComment(Integer.parseInt(request.getParameter("comment_id")));
		comment.setTextsub(helper.TextChange.changeText(comment.getTextsub()));
		request.setAttribute("commentData", comment);
		request.setAttribute("forum_id", request.getParameter("forum_id"));
		request.getRequestDispatcher("/WEB-INF/jsp/commentupdate.jsp").forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ユーザーデータがない場合はIndexへ飛ばす
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		CommentDataBeans comment = new CommentDataBeans();
		comment = CommentDAO.getComment(Integer.parseInt(request.getParameter("comment_id")));
		if(request.getParameter("commentdelete") != null) {
			CommentDAO.deleteComment(comment);
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
		CommentDataBeans com = new CommentDataBeans();
		com.setText(request.getParameter("result"));
		com.setTextsub(request.getParameter("markdown"));
		com.setId(Integer.parseInt(request.getParameter("comment_id")));
		CommentDAO.updateComment(com);
		if(Objects.nonNull(session.getAttribute("pageid"))) {
			String a = String.valueOf(session.getAttribute("pageid"));
			request.setAttribute("page_id", a);
			request.getRequestDispatcher("Page").forward(request, response);
			return;
		}
		else if(Objects.nonNull(request.getParameter("forum_id"))){
			request.setAttribute("forum_id", request.getParameter("forum_id"));
			request.getRequestDispatcher("Forum").forward(request, response);
			return;
		}else{
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}
	}
}
