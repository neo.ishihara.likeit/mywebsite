package proroad;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Pst_SecondaryBeans;
import dao.PstDAO;

/**
 * Servlet implementation class PrimaryCreate
 */
@WebServlet("/SecondaryUpdate")
public class SecondaryUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SecondaryUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("userdata") == null) {
			request.getRequestDispatcher("Index").forward(request, response);
			return;
		}

		Pst_SecondaryBeans pst_s = PstDAO.getSecondarydata(Integer.parseInt(request.getParameter("sid")));
		request.setAttribute("secondary", pst_s);

		request.getRequestDispatcher("/WEB-INF/jsp/secondaryupdate.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		if(request.getParameter("secondarydelele") != null) {
			Pst_SecondaryBeans pst_s = new Pst_SecondaryBeans();
			pst_s.setId(Integer.parseInt(request.getParameter("s_id")));
			PstDAO.deleteSecondarydata(pst_s);
			request.setAttribute("primaryid", request.getParameter("primaryid"));
			request.getRequestDispatcher("PstUpdate").forward(request, response);
			return;
		}


		Pst_SecondaryBeans secondary = new Pst_SecondaryBeans();
		secondary.setId(Integer.parseInt(request.getParameter("s_id")));
		secondary.setTitle(request.getParameter("stitle"));
		secondary.setNumber(Integer.parseInt(request.getParameter("snumber")));

		PstDAO.updateSecondarydata(secondary);
		secondary = PstDAO.getSecondarydata(Integer.parseInt(request.getParameter("s_id")));
		request.setAttribute("primaryid", request.getParameter("primaryid"));
		request.getRequestDispatcher("PstUpdate").forward(request, response);
		return;
	}

}
