package helper;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Angou {
	/*
	 * 受け取った文字を暗号化し返す。
	 */
	public static String angouka(String soosu){
			try {
				String source = soosu;
				Charset charset = StandardCharsets.UTF_8;
				String algorithm = "MD5";
				byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				String result = DatatypeConverter.printHexBinary(bytes);
				return result;
			} catch (NoSuchAlgorithmException |  NullPointerException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			return null;
	}
// テスト用
//	public static void main(String[] argc) throws NoSuchAlgorithmException {
//		String source = "password";
//		Charset charset = StandardCharsets.UTF_8;
//		String algorithm = "MD5";
//
//		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
//		String result = DatatypeConverter.printHexBinary(bytes);
//		System.out.println(result);
//	}
}
