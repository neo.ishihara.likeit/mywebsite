package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ListBeans;

public class ListDAO {
	public static ArrayList<ListBeans> getListByName(String searchWord,int pageNum, int pageMaxCount,int category_id){
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ListBeans> list = new ArrayList<ListBeans>();
		try {
			con = base.DBManager.getConnection();
			//リストの開始する場所を決める
			int startNum = (pageNum - 1) * pageMaxCount;


			if(searchWord.length() == 0 && category_id == 0) {
				//全検索
				st = con.prepareStatement("SELECT * FROM forums LEFT OUTER JOIN pages ON forums.title = pages.title ORDER BY forums.create_date DESC LIMIT ?,?;");
				st.setInt(1, startNum);
				st.setInt(2, pageMaxCount);
			}else if(searchWord.length() == 0 && category_id != 0) {
				//カテゴリーで検索
				st = con.prepareStatement("SELECT * FROM forums LEFT OUTER JOIN pages ON forums.title = pages.title WHERE forums.category_id = ? ORDER BY forums.create_date DESC LIMIT ?,?;");
				st.setInt(1, category_id);
				st.setInt(2, startNum);
				st.setInt(3, pageMaxCount);
			}else if(searchWord.length() != 0 && category_id ==0) {
				//名前で検索
				st = con.prepareStatement("SELECT * FROM forums LEFT OUTER JOIN pages ON forums.title = pages.title WHERE forums.title LIKE ? ORDER BY forums.create_date DESC LIMIT ?,?;");
				st.setString(1, "%"+searchWord+"%");
				st.setInt(2, startNum);
				st.setInt(3, pageMaxCount);
			}else {
				//カテゴリーと名前で検索
				st = con.prepareStatement("SELECT * FROM forums LEFT OUTER JOIN pages ON forums.title = pages.title WHERE forums.title LIKE ? AND forums.category_id = ? ORDER BY forums.create_date DESC LIMIT ?,?;");
				st.setString(1, "%"+searchWord+"%");
				st.setInt(2, category_id);
				st.setInt(3, startNum);
				st.setInt(4, pageMaxCount);
			}

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ListBeans item = new ListBeans();
				item.setTitle(rs.getString("forums.title"));
				item.setQuestion(rs.getInt("question"));
				item.setPage_id(rs.getInt("pages.id"));
				item.setForum_id(rs.getInt("forums.id"));
				item.setCreater_id(rs.getInt("forums.creater_id"));
				item.setCategorydata(CategoryDAO.getCategory(rs.getInt("forums.category_id")));
				item.setCreate_date(rs.getTimestamp("forums.create_date"));
				item.setUserdata(UserDAO.getUserData(rs.getInt("forums.creater_id")));
				list.add(item);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	//リストの総数を調べる
	public static double getListCount(String searchWord,int category_id) {
		Connection con = null;
		PreparedStatement st = null;
		double count = 0.0;
		try {
			con = DBManager.getConnection();
			if(category_id == 0) {
			st = con.prepareStatement("SELECT COUNT(*) AS cnt FROM forums LEFT OUTER JOIN pages ON forums.title = pages.title WHERE forums.title LIKE ?;");
			st.setString(1, "%"+searchWord+"%");
			}else {
				st = con.prepareStatement("SELECT COUNT(*) AS cnt FROM forums LEFT OUTER JOIN pages ON forums.title = pages.title WHERE forums.title LIKE ? AND forums.category_id = ?;");
				st.setString(1, "%"+searchWord+"%");
				st.setInt(2, category_id);
			}
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				count = Double.parseDouble(rs.getString("cnt"));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return count;
	}
}
