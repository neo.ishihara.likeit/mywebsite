package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.PageDataBeans;

public class PageDAO {
	//IDからPageを取得
	public static PageDataBeans getPage(int id) {
		Connection con = null;
		PreparedStatement st = null;
		PageDataBeans page = new PageDataBeans();

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM pages WHERE id = ?;");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				page.setId(rs.getInt("id"));
				page.setTitle(rs.getString("title"));
				page.setText(rs.getString("text"));
				page.setTextsub(rs.getString("textsub"));
				page.setCreater_id(rs.getInt("creater_id"));
				page.setCategory_id(rs.getInt("category_id"));
				page.setCreate_date(rs.getDate("create_date"));
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}

		return page;
	}

	//最後のPageだけを取得
	public static PageDataBeans getLastPage() {
		Connection con = null;
		PreparedStatement st = null;
		PageDataBeans page = new PageDataBeans();

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM pages ORDER BY id DESC LIMIT 1;");
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				page.setId(rs.getInt("id"));
				page.setTitle(rs.getString("title"));
				page.setText(rs.getString("text"));
				page.setCreater_id(rs.getInt("creater_id"));
				page.setCategory_id(rs.getInt("category_id"));
				page.setCreate_date(rs.getDate("create_date"));
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}

		return page;
	}

	//Pageを追加
	public static PageDataBeans setPage(PageDataBeans pagedata) {
		PageDataBeans page = new PageDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO pages VALUE (null,?,?,?,?,?,now());");
			st.setString(1, pagedata.getTitle());
			st.setString(2, pagedata.getText());
			st.setString(3, pagedata.getTextsub());
			st.setInt(4, pagedata.getCreater_id());
			st.setInt(5, pagedata.getCategory_id());

			int rs = st.executeUpdate();
			if (rs < 1) {
				page = null;
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return page;
	}

	//Pageを更新
	public static void updatePage(PageDataBeans pagedata) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("UPDATE pages SET title = ? , text = ?, textsub = ? ,category_id = ? WHERE id = ?;");
			st.setString(1, pagedata.getTitle());
			st.setString(2, pagedata.getText());
			st.setString(3, pagedata.getTextsub());
			st.setInt(4, pagedata.getCategory_id());
			st.setInt(5, pagedata.getId());


			st.executeUpdate();

			st.close();
			con.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//Pageを削除
	public static void deletePage(PageDataBeans pagedata) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM pages WHERE id = ?;");
			st.setInt(1, pagedata.getId());

			st.executeUpdate();

			st.close();
			con.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}
}
