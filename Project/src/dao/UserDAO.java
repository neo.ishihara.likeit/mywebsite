package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.IconDataBeans;
import beans.UserDataBeans;

public class UserDAO {
	/*
	 * ユーザーの登録
	 */
	public static boolean signinUser(UserDataBeans user){
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO users VALUES(null,?,?,?,?,1,2);");
			st.setString(1, user.getLoginid());
			st.setString(2, helper.Angou.angouka(user.getPassword()));
			st.setString(3, user.getName());
			st.setDate(4, user.getCreatedate());
			int rs = st.executeUpdate();
			if (rs > 0)
			con.close();
			st.close();
			return true;
		}catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	/*
	 * 受け取ったパスワードとログインIDを受け取り合っていた場合ユーザーデータを返す。
	 */
	public static UserDataBeans login(String loginid, String pass){
		UserDataBeans user = new UserDataBeans();

		pass = helper.Angou.angouka(pass);
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM users WHERE login_id = ? AND password = ?;");
			st.setString(1, loginid);
			st.setString(2, pass);
			ResultSet rs = st.executeQuery();
			if(rs.next() == true)
			{
				user.setId(rs.getInt("id"));
				user.setLoginid(rs.getString("login_id"));
				user.setPassword("");
				user.setName(rs.getString("name"));
				user.setCreatedate(rs.getDate("create_date"));
				user.setIcon_method_id(rs.getInt("icon_method_id"));
				user.setAuthority_method_id(rs.getInt("authority_method_id"));
			}else {
				user = null;
			}

			st.close();
			con.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}


		return user;
	}

	/*
	 * ユーザー情報の更新
	 */
	public static UserDataBeans updateuser(UserDataBeans user){
		UserDataBeans upuser = user;

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();

			//更新
			if(user.getPassword() != null) {
				st = con.prepareStatement("UPDATE users SET authority_method_id = ? , name = ? , password = ? WHERE id = ?;");
				st.setInt(1, user.getIcon_method_id());
				st.setString(2, user.getName());
				st.setString(3, helper.Angou.angouka(user.getPassword()));
				st.setInt(4, user.getId());
			}else {
				st = con.prepareStatement("UPDATE users SET authority_method_id = ? , name = ? WHERE id = ?;");
				st.setInt(1, user.getIcon_method_id());
				st.setString(2, user.getName());
				st.setInt(3, user.getId());
			}

			int rsint = st.executeUpdate();
			if(!(rsint > 0))
			{
				user = null;
				return user;
			}

			//入力されたデータを返す

			st.close();
			con.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}


		return upuser;
	}

	/*
	 * 既存のログインIDと被っていないか
	 */
	public static boolean checkloginid(String loginid) {
		Connection con = null;
		PreparedStatement st = null;
			try {
				con = base.DBManager.getConnection();
				st = con.prepareStatement("SELECT login_id FROM users WHERE login_id = ?;");
				st.setString(1, loginid);
				ResultSet rs = st.executeQuery();
				if(rs.next())
					return true;
				con.close();
				st.close();
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

		return false;
	}
	/*
	 * ユーザーIDからユーザーデータを取得
	 */
	public static UserDataBeans getUserData(int id) {
		UserDataBeans user = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM users WHERE id = ?;");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if(rs.next())
			{
				user.setId(rs.getInt("id"));
				user.setLoginid(rs.getString("login_id"));
				user.setPassword("");
				user.setName(rs.getString("name"));
				user.setCreatedate(rs.getDate("create_date"));
				user.setIcon_method_id(rs.getInt("icon_method_id"));
				user.setAuthority_method_id(rs.getInt("authority_method_id"));
			}else {
				user = null;
			}

			st.close();
			con.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		return user;
	}
	/*
	 * iconidから名前とURLを取得
	 */
	public static IconDataBeans getIconData(int id) {
		Connection con = null;
		PreparedStatement st = null;
		IconDataBeans icon = new IconDataBeans();
		try {

				con = base.DBManager.getConnection();
				st = con.prepareStatement("SELECT * FROM icon_methods WHERE id = ?;");
				st.setInt(1, id);
				ResultSet rs = st.executeQuery();
				if(rs.next())
				{
					icon.setId(rs.getInt("id"));
					icon.setName(rs.getString("name"));
					icon.setURL(rs.getString("url"));
				} else {
					icon = null;
				}
				con.close();
				st.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}

		return icon;
	}
	/*
	 * iconidから名前とURLを取得
	 */
	public static String getAuthorityName(int id) {
		Connection con = null;
		PreparedStatement st = null;
		String name = null;
		try {

				con = base.DBManager.getConnection();
				st = con.prepareStatement("SELECT * FROM authority_methods WHERE id = ?;");
				st.setInt(1, id);
				ResultSet rs = st.executeQuery();
				if(rs.next())
				{
					name = rs.getString("name");
				} else {
					name = null;
				}
				con.close();
				st.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}

		return name;
	}
}
