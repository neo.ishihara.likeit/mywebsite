package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.CommentDataBeans;

public class CommentDAO {
	//コメントを取得
	public static CommentDataBeans getComment(int id) {
		Connection con = null;
		PreparedStatement st = null;
		CommentDataBeans comment = new CommentDataBeans();

		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM comments WHERE id = ?;");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				comment.setId(rs.getInt("id"));
				comment.setForum_id(rs.getInt("forum_id"));
				comment.setComment_id(rs.getInt("comment_id"));
				comment.setCreater_id(rs.getInt("creater_id"));
				comment.setText(rs.getString("text"));
				comment.setTextsub(rs.getString("textsub"));
				comment.setCreate_date(rs.getTimestamp("create_date"));
				comment.setUserData(UserDAO.getUserData(rs.getInt("creater_id")));
				comment.setIconData(UserDAO.getIconData(comment.getUserData().getIcon_method_id()));
			} else {
				comment = null;
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return comment;
	}

	//フォーラムIDから最初のコメントを取得
	public static CommentDataBeans getForum1Comment(int forumid) {
		Connection con = null;
		PreparedStatement st = null;
		CommentDataBeans comment = new CommentDataBeans();
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM comments WHERE forum_id = ? ORDER BY id ASC LIMIT 1;");
			st.setInt(1, forumid);
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				comment.setId(rs.getInt("id"));
				comment.setForum_id(rs.getInt("forum_id"));
				comment.setComment_id(rs.getInt("comment_id"));
				comment.setCreater_id(rs.getInt("creater_id"));
				comment.setText(rs.getString("text"));
				comment.setTextsub(rs.getString("textsub"));
				comment.setCreate_date(rs.getTimestamp("create_date"));
				comment.setUserData(UserDAO.getUserData(rs.getInt("creater_id")));
				comment.setIconData(UserDAO.getIconData(comment.getUserData().getIcon_method_id()));
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return comment;
	}

	//フォーラムIDからコメントを降順取得また、コメントに対してコメントがあった場合はそれも追加で。
	public static ArrayList<CommentDataBeans> getForum2Comment(int forumid) {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<CommentDataBeans> clist = new ArrayList<CommentDataBeans>();
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM comments WHERE forum_id = ?;");
			st.setInt(1, forumid);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				CommentDataBeans comment = new CommentDataBeans();
				comment.setId(rs.getInt("id"));
				comment.setForum_id(rs.getInt("forum_id"));
				comment.setComment_id(rs.getInt("comment_id"));
				comment.setCreater_id(rs.getInt("creater_id"));
				comment.setText(rs.getString("text"));
				comment.setTextsub(rs.getString("textsub"));
				comment.setCreate_date(rs.getTimestamp("create_date"));
				comment.setUserData(UserDAO.getUserData(rs.getInt("creater_id")));
				comment.setIconData(UserDAO.getIconData(comment.getUserData().getIcon_method_id()));
				clist.add(comment);
				ArrayList<CommentDataBeans> cslist = new ArrayList<CommentDataBeans>();
				cslist = getComment2Comment(rs.getInt("id"));
				for (CommentDataBeans c : cslist)
					clist.add(c);
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return clist;
	}

	//コメントIDからコメントを降順取得。
	public static ArrayList<CommentDataBeans> getComment2Comment(int id) {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<CommentDataBeans> clist = new ArrayList<CommentDataBeans>();
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM comments WHERE comment_id = ?;");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				CommentDataBeans comment = new CommentDataBeans();
				comment.setId(rs.getInt("id"));
				comment.setForum_id(rs.getInt("forum_id"));
				comment.setComment_id(rs.getInt("comment_id"));
				comment.setCreater_id(rs.getInt("creater_id"));
				comment.setText(rs.getString("text"));
				comment.setTextsub(rs.getString("textsub"));
				comment.setCreate_date(rs.getTimestamp("create_date"));
				comment.setUserData(UserDAO.getUserData(rs.getInt("creater_id")));
				comment.setIconData(UserDAO.getIconData(comment.getUserData().getIcon_method_id()));
				clist.add(comment);
				ArrayList<CommentDataBeans> cslist = new ArrayList<CommentDataBeans>();
				cslist = getComment2Comment(rs.getInt("id"));
				for (CommentDataBeans c : cslist)
					clist.add(c);
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return clist;
	}

	//コメントを追加
	public static void setComment(CommentDataBeans comment) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO comments VALUE (null,?,?,?,?,?,now());");
			st.setInt(1, comment.getForum_id());
			st.setInt(2, comment.getComment_id());
			st.setInt(3, comment.getCreater_id());
			st.setString(4, comment.getText());
			st.setString(5, comment.getTextsub());

			st.executeUpdate();
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	//コメントを更新
	public static void updateComment(CommentDataBeans comment) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement(
					"UPDATE comments SET text = ? ,textsub = ? WHERE id = ?;");
			st.setString(1, comment.getText());
			st.setString(2, comment.getTextsub());
			st.setInt(3, comment.getId());

			st.executeUpdate();
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	//コメントを削除
	public static void deleteComment(CommentDataBeans comment) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM comments WHERE id = ?;");
			st.setInt(1, comment.getId());

			st.executeUpdate();
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
