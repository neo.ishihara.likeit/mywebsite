package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.CategoryBeans;

public class CategoryDAO {
	//idからカテゴリーデータを取得
	public static CategoryBeans getCategory(int id){
		CategoryBeans category = new CategoryBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM categories_method WHERE id = ?");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if(rs.next() == true)
			{
				category.setId(rs.getInt("id"));
				category.setName(rs.getString("name"));
			}else {
				category = null;
			}

			st.close();
			con.close();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}


		return category;
	}
	//カテゴリーデータをすべて取得∴∵∴∵∴∵∴∵∴∵∴∵∴∵∴∵∴∵∴∵∴∵∴∵∴∵
		public static ArrayList<CategoryBeans> getCategory(){
			ArrayList<CategoryBeans> clist = new ArrayList<CategoryBeans>();
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = base.DBManager.getConnection();
				st = con.prepareStatement("SELECT * FROM categories_method");
				ResultSet rs = st.executeQuery();
				while(rs.next() == true){
					CategoryBeans category = new CategoryBeans();
					category.setId(rs.getInt("id"));
					category.setName(rs.getString("name"));
					clist.add(category);
				}

				st.close();
				con.close();
			} catch(SQLException e) {
				e.printStackTrace();
			} finally {
				if(con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
				}
			}


			return clist;
		}
}
