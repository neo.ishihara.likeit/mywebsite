package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.Pst_PrimaryBeans;
import beans.Pst_SecondaryBeans;
import beans.Pst_TertiaryBeans;

public class PstDAO {

	//全取得

	//プライマリーの全てを取得
	public static ArrayList<Pst_PrimaryBeans> getAllPrimarydata() {
		ArrayList<Pst_PrimaryBeans> plist = new ArrayList<Pst_PrimaryBeans>();

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM primaries;");
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				Pst_PrimaryBeans primary = new Pst_PrimaryBeans();

				primary.setId(rs.getInt("id"));
				primary.setTitle(rs.getString("title"));
				primary.setDetail(rs.getString("detail"));
				primary.setCreate_date(rs.getDate("create_date"));
				primary.setCreater_id(rs.getInt("creater_id"));

				plist.add(primary);
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
		}
		return plist;
	}

	//ターシャリーの全てを取得
	public static ArrayList<Pst_TertiaryBeans> getAllTertiary() {
		ArrayList<Pst_TertiaryBeans> tertiary = new ArrayList<Pst_TertiaryBeans>();

		Connection con;
		PreparedStatement st;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM tertiaries");
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				Pst_TertiaryBeans pst = new Pst_TertiaryBeans();

				pst.setId(rs.getInt("id"));
				pst.setSecondary_id(rs.getInt("secondary_id"));
				pst.setTitle(rs.getString("title"));
				pst.setNumber(rs.getInt("number"));
				pst.setPage_id(rs.getInt("page_id"));

				tertiary.add(pst);
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tertiary;

	}

	//単体取得

	//プライマリ―を単体取得
	public static Pst_PrimaryBeans getPrimarydata(int primary_id) {
		Pst_PrimaryBeans primary = new Pst_PrimaryBeans();

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM primaries WHERE id = ?");
			st.setInt(1, primary_id);
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				primary.setId(rs.getInt("id"));
				primary.setTitle(rs.getString("title"));
				primary.setDetail(rs.getString("detail"));
				primary.setCreate_date(rs.getDate("create_date"));
				primary.setCreater_id(rs.getInt("creater_id"));
			} else {
				primary = null;
			}
			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return primary;
	}

	//セカンダリーを単体取得
	public static Pst_SecondaryBeans getSecondarydata(int secondary_id) {
		Pst_SecondaryBeans secondary = new Pst_SecondaryBeans();

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM secondaries WHERE id = ?");
			st.setInt(1, secondary_id);
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				secondary.setId(rs.getInt("id"));
				secondary.setPrimary_id(rs.getInt("primary_id"));
				secondary.setTitle(rs.getString("title"));
				secondary.setNumber(rs.getInt("number"));
			} else {
				secondary = null;
			}
			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return secondary;
	}

	//セカンダリーを単体取得
	public static Pst_TertiaryBeans getTertiarydata(int tertiary_id) {
		Pst_TertiaryBeans tertiary = new Pst_TertiaryBeans();

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM tertiaries WHERE id = ?");
			st.setInt(1, tertiary_id);
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				tertiary.setId(rs.getInt("id"));
				tertiary.setSecondary_id(rs.getInt("secondary_id"));
				tertiary.setTitle(rs.getString("title"));
				tertiary.setNumber(rs.getInt("number"));
				tertiary.setPage_id(rs.getInt("page_id"));
			} else {
				tertiary = null;
			}
			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tertiary;
	}

	//以下の全取得

	//プライマリーのIDを取得したらセカンダリーのリストをnumber順に出す
	public static ArrayList<Pst_SecondaryBeans> getAllSecondarydeta(int primary_id) {
		ArrayList<Pst_SecondaryBeans> secondary = new ArrayList<Pst_SecondaryBeans>();

		Connection con;
		PreparedStatement st;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM secondaries WHERE primary_id = ? ORDER BY number ASC;");
			st.setInt(1, primary_id);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				Pst_SecondaryBeans pst = new Pst_SecondaryBeans();

				pst.setId(rs.getInt("id"));
				pst.setPrimary_id(rs.getInt("primary_id"));
				pst.setTitle(rs.getString("title"));
				pst.setNumber(rs.getInt("number"));
				secondary.add(pst);
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return secondary;
	}

	//セカンダリーのIDを取得したらターシャリーのリストをnumber順に出す
	public static ArrayList<Pst_TertiaryBeans> getAllTertiarydeta(int secondary_id) {
		ArrayList<Pst_TertiaryBeans> tertiary = new ArrayList<Pst_TertiaryBeans>();

		Connection con;
		PreparedStatement st;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM tertiaries WHERE secondary_id = 1 ORDER BY number ASC;");
			st.setInt(1, secondary_id);
			ResultSet rs = st.executeQuery();

			if (!(rs.next()))
				return null;

			while (rs.next()) {
				Pst_TertiaryBeans pst = new Pst_TertiaryBeans();

				pst.setId(rs.getInt("id"));
				pst.setSecondary_id(rs.getInt("secondary_id"));
				pst.setTitle(rs.getString("title"));
				pst.setNumber(rs.getInt("number"));
				pst.setPage_id(rs.getInt("page_id"));
				tertiary.add(pst);
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tertiary;
	}

	//プライマリー

	//プライマリーをセットする
	public static void setPrimarydata(Pst_PrimaryBeans primarydata) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO primaries VALUES (null,?,?,now(),?);");
			st.setString(1, primarydata.getTitle());
			st.setString(2, primarydata.getDetail());
			st.setInt(3, primarydata.getCreater_id());

			st.executeUpdate();

			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//プライマリーをアップデートする
	public static void updatePrimarydata(Pst_PrimaryBeans primarydata) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();

			//更新
			st = con.prepareStatement("UPDATE primaries SET title = ? ,detail = ? WHERE id = ?;");
			st.setString(1, primarydata.getTitle());
			st.setString(2, primarydata.getDetail());
			st.setInt(3, primarydata.getId());

			st.executeUpdate();

			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//プライマリーをデリートする
	public static void deletePrimarydata(Pst_PrimaryBeans primarydata) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();

			//更新
			st = con.prepareStatement("DELETE FROM primaries WHERE id = ?;");
			st.setInt(1, primarydata.getId());

			st.executeUpdate();

			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//セカンダリー

	//セカンダリーをセットする
	public static void setSecondarydata(Pst_SecondaryBeans secondary) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO secondaries VALUES (null,?,?,?);");
			st.setInt(1, secondary.getPrimary_id());
			st.setString(2, secondary.getTitle());
			st.setInt(3, secondary.getNumber());

			st.executeUpdate();

			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//セカンダリーをアップデートする
	public static void updateSecondarydata(Pst_SecondaryBeans secondary) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();

			//更新
			st = con.prepareStatement("UPDATE secondaries SET title = ? ,number = ? WHERE id = ?;");
			st.setString(1, secondary.getTitle());
			st.setInt(2, secondary.getNumber());
			st.setInt(3, secondary.getId());

			st.executeUpdate();

			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//セカンダリーをデリートする
	public static void deleteSecondarydata(Pst_SecondaryBeans secondary) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();

			//更新
			st = con.prepareStatement("DELETE FROM secondaries WHERE id = ?;");
			st.setInt(1, secondary.getId());

			st.executeUpdate();

			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//ターシャリー

	//ターシャリーをセットする
	public static void setTertiarydata(Pst_TertiaryBeans tertiary) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO tertiaries VALUES (null,?,?,?,?);");
			st.setInt(1, tertiary.getSecondary_id());
			st.setString(2, tertiary.getTitle());
			st.setInt(3, tertiary.getNumber());
			st.setInt(4, tertiary.getPage_id());

			st.executeUpdate();

			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//ターシャリーをアップデートする
	public static void updateTertiarydata(Pst_TertiaryBeans tertiary) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();

			//更新
			st = con.prepareStatement("UPDATE tertiaries SET title = ? ,number = ? , page_id = ? WHERE id = ?;");
			st.setString(1, tertiary.getTitle());
			st.setInt(2, tertiary.getNumber());
			st.setInt(3, tertiary.getPage_id());
			st.setInt(4, tertiary.getId());

			st.executeUpdate();

			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

	//ターシャリーをデリートする
	public static void deleteTertiarydata(Pst_TertiaryBeans tertiary) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();

			//更新
			st = con.prepareStatement("DELETE FROM tertiaries WHERE id = ?;");
			st.setInt(1, tertiary.getId());

			st.executeUpdate();

			st.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
	}

}
