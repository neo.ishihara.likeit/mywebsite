package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import base.DBManager;
import beans.ForumBeans;

public class ForumDAO {

	//掲示板を取得
	public static ForumBeans getForum(int forum_id) {
		ForumBeans forum = new ForumBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM forums WHERE id = ?;");
			st.setInt(1, forum_id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				forum.setId(rs.getInt("id"));
				forum.setTitle(rs.getString("title"));
				forum.setPage_id(rs.getInt("page_id"));
				forum.setCategory_id(rs.getInt("category_id"));
				forum.setQuestion(rs.getInt("question"));
				forum.setCreater_id(rs.getInt("creater_id"));
				forum.setCreate_date(rs.getTimestamp("create_date"));
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return forum;
	}

	//ページIDから掲示板を取得
	public static ForumBeans getPage_id2Forum(int page_id) {
		ForumBeans forum = new ForumBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM forums WHERE page_id = ?;");
			st.setInt(1, page_id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				forum.setId(rs.getInt("id"));
				forum.setTitle(rs.getString("title"));
				forum.setPage_id(rs.getInt("page_id"));
				forum.setCategory_id(rs.getInt("category_id"));
				forum.setQuestion(rs.getInt("question"));
				forum.setCreater_id(rs.getInt("creater_id"));
				forum.setCreate_date(rs.getTimestamp("create_date"));
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return forum;
	}

	//最後のForumだけを取得
	public static ForumBeans getLastForum() {
		Connection con = null;
		PreparedStatement st = null;
		ForumBeans forum = new ForumBeans();

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM forums ORDER BY id DESC LIMIT 1;");
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				forum.setId(rs.getInt("id"));
				forum.setTitle(rs.getString("title"));
				forum.setPage_id(rs.getInt("page_id"));
				forum.setCategory_id(rs.getInt("category_id"));
				forum.setQuestion(rs.getInt("question"));
				forum.setCreater_id(rs.getInt("creater_id"));
				forum.setCreate_date(rs.getTimestamp("create_date"));
			}
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}

		return forum;
	}

	//掲示板を作成
	public static void setForum(ForumBeans forum) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO forums VALUE (null,?,?,?,?,?,now());");
			st.setString(1, forum.getTitle());
			if (Objects.nonNull(forum.getPage_id()))
				st.setInt(2, forum.getPage_id());
			else
				st.setInt(2, 0);
			st.setInt(3, forum.getCategory_id());
			st.setInt(4, forum.getQuestion());
			st.setInt(5, forum.getCreater_id());

			st.executeUpdate();
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	//掲示板を更新
	public static void updateForum(ForumBeans forum) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("UPDATE forums SET title = ? , category_id = ?, question = ? WHERE id = ?;");
			st.setString(1, forum.getTitle());
			st.setInt(2, forum.getCategory_id());
			st.setInt(3, forum.getQuestion());
			st.setInt(4, forum.getId());

			st.executeUpdate();
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	//掲示板を削除
	public static void deleteForum(int forum_id) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = base.DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM forums WHERE id = ?;");
			st.setInt(1, forum_id);

			st.executeUpdate();
			con.close();
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null || st != null)
				try {
					con.close();
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
