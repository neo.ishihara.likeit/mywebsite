package beans;

public class Pst_TertiaryBeans {
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSecondary_id() {
		return secondary_id;
	}
	public void setSecondary_id(int secondary_id) {
		this.secondary_id = secondary_id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getPage_id() {
		return page_id;
	}
	public void setPage_id(int page_id) {
		this.page_id = page_id;
	}
	private int id;
	private int secondary_id;
	private int number;
	private String title;
	private int page_id;
}
