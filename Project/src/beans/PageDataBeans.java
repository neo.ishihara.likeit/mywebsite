package beans;

import java.sql.Date;

public class PageDataBeans {
	private int id;
	private String title;
	private String text;
	private String textsub;
	private int creater_id;
	private int category_id;
	private Date create_date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTextsub() {
		return textsub;
	}
	public void setTextsub(String textsub) {
		this.textsub = textsub;
	}
	public int getCreater_id() {
		return creater_id;
	}
	public void setCreater_id(int creater_id) {
		this.creater_id = creater_id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
}
