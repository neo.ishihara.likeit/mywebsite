package beans;

public class Pst_SecondaryBeans {
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPrimary_id() {
		return primary_id;
	}
	public void setPrimary_id(int primary_id) {
		this.primary_id = primary_id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	private int id;
	private int primary_id;
	private int number;
	private String title;
}
