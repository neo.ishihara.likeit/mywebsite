package beans;

import java.sql.Timestamp;

public class CommentDataBeans {
	private int id;
	private int forum_id;
	private int comment_id;
	private int creater_id;
	private String text;
	private String textsub;
	private Timestamp create_date;
	private UserDataBeans userData;
	private IconDataBeans iconData;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getForum_id() {
		return forum_id;
	}
	public void setForum_id(int forum_id) {
		this.forum_id = forum_id;
	}
	public int getComment_id() {
		return comment_id;
	}
	public void setComment_id(int comment_id) {
		this.comment_id = comment_id;
	}
	public int getCreater_id() {
		return creater_id;
	}
	public void setCreater_id(int creater_id) {
		this.creater_id = creater_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTextsub() {
		return textsub;
	}
	public void setTextsub(String textsub) {
		this.textsub = textsub;
	}
	public Timestamp getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Timestamp create_date) {
		this.create_date = create_date;
	}
	public UserDataBeans getUserData() {
		return userData;
	}
	public void setUserData(UserDataBeans userData) {
		this.userData = userData;
	}
	public IconDataBeans getIconData() {
		return iconData;
	}
	public void setIconData(IconDataBeans iconData) {
		this.iconData = iconData;
	}
}
