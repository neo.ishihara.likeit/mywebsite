package beans;

import java.sql.Date;

public class UserDataBeans {
	private int id;
	private String loginid;
	private String password;
	private String name;
	private Date createdate;
	private int icon_method_id;
	private int authority_method_id;

	public int getId() {
		return id;
	}
	public void setId(int i) {
		this.id = i;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date sqlDate) {
		this.createdate = sqlDate;
	}
	public int getIcon_method_id() {
		return icon_method_id;
	}
	public void setIcon_method_id(int icon_method_id) {
		this.icon_method_id = icon_method_id;
	}
	public int getAuthority_method_id() {
		return authority_method_id;
	}
	public void setAuthority_method_id(int authority_method_id) {
		this.authority_method_id = authority_method_id;
	}
}
