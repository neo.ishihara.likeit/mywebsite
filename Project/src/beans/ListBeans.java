package beans;

import java.sql.Timestamp;

public class ListBeans {
	private int page_id;
	private int forum_id;
	private String title;
	private CategoryBeans categorydata;
	private int creater_id;
	private Timestamp create_date;
	private int question;
	private UserDataBeans userdata;
	public int getPage_id() {
		return page_id;
	}
	public void setPage_id(int page_id) {
		this.page_id = page_id;
	}
	public int getForum_id() {
		return forum_id;
	}
	public void setForum_id(int forum_id) {
		this.forum_id = forum_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getCreater_id() {
		return creater_id;
	}
	public void setCreater_id(int creater_id) {
		this.creater_id = creater_id;
	}
	public Timestamp getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Timestamp create_date) {
		this.create_date = create_date;
	}
	public int getQuestion() {
		return question;
	}
	public void setQuestion(int question) {
		this.question = question;
	}
	public UserDataBeans getUserdata() {
		return userdata;
	}
	public void setUserdata(UserDataBeans userdata) {
		this.userdata = userdata;
	}
	public CategoryBeans getCategorydata() {
		return categorydata;
	}
	public void setCategorydata(CategoryBeans categorydata) {
		this.categorydata = categorydata;
	}

}
